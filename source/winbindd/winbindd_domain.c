/*
   Unix SMB/CIFS implementation.

   Winbind domain child functions

   Copyright (C) Stefan Metzmacher 2007

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "winbindd.h"

#undef DBGC_CLASS
#define DBGC_CLASS DBGC_WINBIND

static const struct winbindd_child_dispatch_table domain_dispatch_table[];

void setup_domain_child(struct winbindd_domain *domain,
			struct winbindd_child *child)
{
	setup_child(child, domain_dispatch_table,
		    "log.wb", domain->name);

	child->domain = domain;
}

static const struct winbindd_child_dispatch_table domain_dispatch_table[] = {
	{
		.name		= "NDR_WINBIND_LOOKUP",
		.ndr_opnum	= NDR_WINBIND_LOOKUP,
		.ndr_fn		= winbindd_ndr_domain_child_lookup,
	},{
		.name		= "NDR_WINBIND_TRUST",
		.ndr_opnum	= NDR_WINBIND_TRUST,
		.ndr_fn		= winbindd_ndr_domain_child_trust,
	},{
		.name		= "NDR_WINBIND_AUTH",
		.ndr_opnum	= NDR_WINBIND_AUTH,
		.ndr_fn		= winbindd_ndr_domain_child_auth,
	},{
		.name		= "PAM_AUTH",
		.struct_cmd	= WINBINDD_PAM_AUTH,
		.struct_fn	= winbindd_dual_pam_auth,
	},{
		.name		= "AUTH_CRAP",
		.struct_cmd	= WINBINDD_PAM_AUTH_CRAP,
		.struct_fn	= winbindd_dual_pam_auth_crap,
	},{
		.name		= "PAM_LOGOFF",
		.struct_cmd	= WINBINDD_PAM_LOGOFF,
		.struct_fn	= winbindd_dual_pam_logoff,
	},{
		.name		= "CHNG_PSWD_AUTH_CRAP",
		.struct_cmd	= WINBINDD_PAM_CHNG_PSWD_AUTH_CRAP,
		.struct_fn	= winbindd_dual_pam_chng_pswd_auth_crap,
	},{
		.name		= "PAM_CHAUTHTOK",
		.struct_cmd	= WINBINDD_PAM_CHAUTHTOK,
		.struct_fn	= winbindd_dual_pam_chauthtok,
	},{
		.name		= "CCACHE_NTLM_AUTH",
		.struct_cmd	= WINBINDD_CCACHE_NTLMAUTH,
		.struct_fn	= winbindd_dual_ccache_ntlm_auth,
	},{
		.name		= "PING",
		.struct_cmd	= WINBINDD_PING,
		.struct_fn	= winbindd_dual_ping,
		.ndr_opnum	= NDR_WINBIND_PING,
		.ndr_fn		= winbindd_ndr_child_ping,
	},{
		.name		= "NDR_WINBIND_GET_DOMAIN_INFO",
		.ndr_opnum 	= NDR_WINBIND_GET_DOMAIN_INFO,
		.ndr_fn		= winbindd_ndr_child_get_domain_info,
	},{
		.name		= "NDR_WINBIND_GET_DC_INFO",
		.ndr_opnum	= NDR_WINBIND_GET_DC_INFO,
		.ndr_fn		= winbindd_ndr_domain_child_get_dc_info,
	},{
		.name		= NULL,
	}
};
