/*
   Unix SMB/CIFS implementation.

   Async helpers for blocking functions

   Copyright (C) Volker Lendecke 2005
   Copyright (C) Gerald Carter 2006
   Copyright (C) Simo Sorce 2007

   The helpers always consist of three functions:

   * A request setup function that takes the necessary parameters together
     with a continuation function that is to be called upon completion

   * A private continuation function that is internal only. This is to be
     called by the lower-level functions in do_async(). Its only task is to
     properly call the continuation function named above.

   * A worker function that is called inside the appropriate child process.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "winbindd.h"

#undef DBGC_CLASS
#define DBGC_CLASS DBGC_WINBIND

static const struct winbindd_child_dispatch_table idmap_dispatch_table[];

static struct winbindd_child static_idmap_child;

void init_idmap_child(void)
{
	setup_child(&static_idmap_child,
		    idmap_dispatch_table,
		    "log.winbindd", "idmap");
}

struct winbindd_child *idmap_child(void)
{
	return &static_idmap_child;
}

static void ndr_child_get_idmap_sid2uid(struct winbindd_domain *domain,
					struct winbindd_cli_state *state,
					struct winbind_get_idmap *r)
{
	uid_t uid;
	NTSTATUS result;

	DEBUG(3, ("sid to uid '%s'\n", sid_string_tos(r->in.req.sid)));

	/* Find uid for this sid and return it, possibly ask the slow remote idmap */

	result = idmap_sid_to_uid(r->in.req.sid, &uid);
	if (!NT_STATUS_IS_OK(result)) {
		DEBUG(1, ("Can't map '%s' to uid: %s\n",
			sid_string_tos(r->in.req.sid),
			nt_errstr(result)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	DEBUG(10, ("sid %s mapped to uid %u\n",
		sid_string_tos(r->in.req.sid), uid));

	r->out.rep->uid = uid;
	r->out.result = WINBIND_STATUS_OK;
}

static void ndr_child_get_idmap_sid2gid(struct winbindd_domain *domain,
					struct winbindd_cli_state *state,
					struct winbind_get_idmap *r)
{
	gid_t gid;
	NTSTATUS result;

	DEBUG(3, ("sid to gid '%s'\n", sid_string_tos(r->in.req.sid)));

	/* Find uid for this sid and return it, possibly ask the slow remote idmap */

	result = idmap_sid_to_gid(r->in.req.sid, &gid);
	if (!NT_STATUS_IS_OK(result)) {
		DEBUG(1, ("Can't map '%s' to gid: %s\n",
			sid_string_tos(r->in.req.sid),
			nt_errstr(result)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	DEBUG(10, ("sid %s mapped to gid %u\n",
		sid_string_tos(r->in.req.sid), gid));

	r->out.rep->gid = gid;
	r->out.result = WINBIND_STATUS_OK;
}

static void ndr_child_get_idmap_uid2sid(struct winbindd_domain *domain,
					struct winbindd_cli_state *state,
					struct winbind_get_idmap *r)
{
	DOM_SID sid;
	uid_t uid;
	NTSTATUS result;

	DEBUG(3, ("uid to sid '%llu'\n",
		(unsigned long long)r->in.req.uid));

	/* the IDMAP subsystem only knows about uint32_t id's yet */
	if (r->in.req.uid > UINT32_MAX) {
		DEBUG(1, ("Can't map uid '%llu' to sid\n",
			(unsigned long long)r->in.req.uid));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	/* Find uid for this sid and return it, possibly ask the slow remote idmap */

	uid = r->in.req.uid;

	result = idmap_uid_to_sid(&sid, uid);
	if (!NT_STATUS_IS_OK(result)) {
		DEBUG(1, ("Can't map uid '%u' to sid: %s\n",
			uid, nt_errstr(result)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	DEBUG(10, ("uid %u mapped to sid %s\n",
		uid, sid_string_tos(&sid)));

	r->out.rep->sid = sid_dup_talloc(r, &sid);
	if (!r->out.rep->sid) {
		r->out.result = WINBIND_STATUS_NO_MEMORY;
		return;
	}

	r->out.result = WINBIND_STATUS_OK;
}

static void ndr_child_get_idmap_gid2sid(struct winbindd_domain *domain,
					struct winbindd_cli_state *state,
					struct winbind_get_idmap *r)
{
	DOM_SID sid;
	gid_t gid;
	NTSTATUS result;

	DEBUG(3, ("gid to sid '%llu'\n",
		(unsigned long long)r->in.req.gid));

	/* the IDMAP subsystem only knows about uint32_t id's yet */
	if (r->in.req.gid > UINT32_MAX) {
		DEBUG(1, ("Can't map gid '%llu' to sid\n",
			(unsigned long long)r->in.req.gid));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	/* Find uid for this sid and return it, possibly ask the slow remote idmap */
	gid = r->in.req.gid;

	result = idmap_gid_to_sid(&sid, gid);
	if (!NT_STATUS_IS_OK(result)) {
		DEBUG(1, ("Can't map gid '%u' to sid: %s\n",
			gid, nt_errstr(result)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	DEBUG(10, ("gid %u mapped to sid %s\n",
		gid, sid_string_tos(&sid)));

	r->out.rep->sid = sid_dup_talloc(r, &sid);
	if (!r->out.rep->sid) {
		r->out.result = WINBIND_STATUS_NO_MEMORY;
		return;
	}

	r->out.result = WINBIND_STATUS_OK;
}

void winbindd_ndr_child_get_idmap(struct winbindd_domain *domain,
				  struct winbindd_cli_state *state)
{
	struct winbind_get_idmap *r;

	r = talloc_get_type_abort(state->c.ndr.r,
				  struct winbind_get_idmap);

	switch (*r->in.level) {
	case WINBIND_IDMAP_LEVEL_SID_TO_UID:
		ndr_child_get_idmap_sid2uid(domain, state, r);
		return;

	case WINBIND_IDMAP_LEVEL_SID_TO_GID:
		ndr_child_get_idmap_sid2gid(domain, state, r);
		return;

	case WINBIND_IDMAP_LEVEL_UID_TO_SID:
		ndr_child_get_idmap_uid2sid(domain, state, r);
		return;

	case WINBIND_IDMAP_LEVEL_GID_TO_SID:
		ndr_child_get_idmap_gid2sid(domain, state, r);
		return;
	}

	r->out.result = WINBIND_STATUS_UNKNOWN_LEVEL;
	return;
}

static void ndr_child_set_idmap_allocate_uid(struct winbindd_domain *domain,
					     struct winbindd_cli_state *state,
					     struct winbind_set_idmap *r)
{
	NTSTATUS result;
	struct unixid xid;

	DEBUG(3, ("allocate_uid\n"));

	result = idmap_allocate_uid(&xid);
	if (!NT_STATUS_IS_OK(result)) {
		DEBUG(1,("Can't allocate uid: %s\n",
			 nt_errstr(result)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	DEBUG(10, ("allocte_uid: %u\n",
		   xid.id));

	r->out.rep->uid = xid.id;

	r->out.result = WINBIND_STATUS_OK;
}

static void ndr_child_set_idmap_allocate_gid(struct winbindd_domain *domain,
					     struct winbindd_cli_state *state,
					     struct winbind_set_idmap *r)
{
	NTSTATUS result;
	struct unixid xid;

	DEBUG(3, ("allocate_gid\n"));

	result = idmap_allocate_gid(&xid);
	if (!NT_STATUS_IS_OK(result)) {
		DEBUG(1,("Can't allocate gid: %s\n",
			 nt_errstr(result)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	DEBUG(10, ("allocte_gid: %u\n",
		   xid.id));

	r->out.rep->gid = xid.id;

	r->out.result = WINBIND_STATUS_OK;
}

static void ndr_child_set_idmap_set_mapping(struct winbindd_domain *domain,
					    struct winbindd_cli_state *state,
					    struct winbind_set_idmap *r)
{
	NTSTATUS result;

	DEBUG(3,("set_mapping: sid %s -> %u type:%u\n",
		 sid_string_tos(r->in.req.mapping.sid),
		 r->in.req.mapping.xid.id,
		 r->in.req.mapping.xid.type));

	result = idmap_set_mapping(&r->in.req.mapping);
	if (!NT_STATUS_IS_OK(result)) {
		DEBUG(1,("Can't set mapping: %s\n",
			 nt_errstr(result)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	r->out.result = WINBIND_STATUS_OK;
}

static void ndr_child_set_idmap_set_hwm(struct winbindd_domain *domain,
					struct winbindd_cli_state *state,
					struct winbind_set_idmap *r)
{
	NTSTATUS result;

	DEBUG(3,("set_hwm: %u type:%u\n",
		 r->in.req.hwm.id,
		 r->in.req.hwm.type));

	switch (r->in.req.hwm.type) {
	case ID_TYPE_UID:
		result = idmap_set_uid_hwm(&r->in.req.hwm);
		break;
	case ID_TYPE_GID:
		result = idmap_set_gid_hwm(&r->in.req.hwm);
		break;
	default:
		result = NT_STATUS_INVALID_PARAMETER;
		break;
	}

	if (!NT_STATUS_IS_OK(result)) {
		DEBUG(1,("Can't set hwm: %s\n",
			 nt_errstr(result)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	r->out.result = WINBIND_STATUS_OK;
}

void winbindd_ndr_child_set_idmap(struct winbindd_domain *domain,
				  struct winbindd_cli_state *state)
{
	struct winbind_set_idmap *r;

	r = talloc_get_type_abort(state->c.ndr.r,
				  struct winbind_set_idmap);

	switch (*r->in.level) {
	case WINBIND_SET_IDMAP_LEVEL_ALLOCATE_UID:
		ndr_child_set_idmap_allocate_uid(domain, state, r);
		return;

	case WINBIND_SET_IDMAP_LEVEL_ALLOCATE_GID:
		ndr_child_set_idmap_allocate_gid(domain, state, r);
		return;

	case WINBIND_SET_IDMAP_LEVEL_SET_MAPPING:
		ndr_child_set_idmap_set_mapping(domain, state, r);
		return;

	case WINBIND_SET_IDMAP_LEVEL_SET_HWM:
		ndr_child_set_idmap_set_hwm(domain, state, r);
		return;
	}

	r->out.result = WINBIND_STATUS_UNKNOWN_LEVEL;
	return;
}

static void winbindd_sid2uid_recv(TALLOC_CTX *mem_ctx, bool success,
				    struct winbindd_ndr_call *c,
				    void *_r,
				    void *_cont,
				    void *private_data)
{
	void (*cont)(void *priv, bool succ, uid_t) =
		(void (*)(void *, bool, uid_t))_cont;
	struct winbind_get_idmap *r =
		talloc_get_type_abort(_r, struct winbind_get_idmap);
	uid_t uid;

	if (!success) {
		DEBUG(5, ("Could not get_idmap(sid2uid)\n"));
		TALLOC_FREE(r);
		cont(private_data, false, (uid_t)-1);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("get_idmap(sid2uid) returned an error:0x%08X\n",
			r->out.result));
		TALLOC_FREE(r);
		cont(private_data, false, (uid_t)-1);
		return;
	}

	if (r->out.rep->uid > UINT32_MAX) {
		DEBUG(1, ("get_idmap(sid2uid) returned a 64bit uid %llu\n",
			(unsigned long long)r->out.rep->uid));
		TALLOC_FREE(r);
		cont(private_data, false, (uid_t)-1);
		return;
	}

	uid = r->out.rep->uid;
	TALLOC_FREE(r);
	cont(private_data, true, uid);
}

void winbindd_sid2uid_async(TALLOC_CTX *mem_ctx, const DOM_SID *sid,
			 void (*cont)(void *private_data, bool success, uid_t uid),
			 void *private_data)
{
	struct winbind_get_idmap *r = NULL;

	DEBUG(7,("winbindd_sid2uid_async: Resolving %s to a uid\n",
		sid_string_tos(sid)));

	r = TALLOC_P(mem_ctx, struct winbind_get_idmap);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_get_idmap_level);
	if (!r->in.level) goto nomem;

	*r->in.level	= WINBIND_IDMAP_LEVEL_SID_TO_UID;
	r->in.req.sid	= discard_const(sid);

	do_async_ndr(mem_ctx, idmap_child(),
		     NDR_WINBIND_GET_IDMAP, r,
		     winbindd_sid2uid_recv, r,
		     (void *)cont, private_data);
	return;
nomem:
	TALLOC_FREE(r);
	cont(private_data, false, (uid_t)-1);
	return;
}

static void winbindd_sid2gid_recv(TALLOC_CTX *mem_ctx, bool success,
				    struct winbindd_ndr_call *c,
				    void *_r,
				    void *_cont,
				    void *private_data)
{
	void (*cont)(void *priv, bool succ, gid_t) =
		(void (*)(void *, bool, gid_t))_cont;
	struct winbind_get_idmap *r =
		talloc_get_type_abort(_r, struct winbind_get_idmap);
	gid_t gid;

	if (!success) {
		DEBUG(5, ("Could not get_idmap(sid2gid)\n"));
		TALLOC_FREE(r);
		cont(private_data, false, (gid_t)-1);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("get_idmap(sid2gid) returned an error:0x%08X\n",
			r->out.result));
		TALLOC_FREE(r);
		cont(private_data, false, (gid_t)-1);
		return;
	}

	if (r->out.rep->gid > UINT32_MAX) {
		DEBUG(1, ("get_idmap(sid2gid) returned a 64bit gid %llu\n",
			(unsigned long long)r->out.rep->gid));
		TALLOC_FREE(r);
		cont(private_data, false, (gid_t)-1);
		return;
	}

	gid = r->out.rep->gid;
	TALLOC_FREE(r);
	cont(private_data, true, gid);
}

void winbindd_sid2gid_async(TALLOC_CTX *mem_ctx, const DOM_SID *sid,
			 void (*cont)(void *private_data, bool success, gid_t gid),
			 void *private_data)
{
	struct winbind_get_idmap *r = NULL;

	DEBUG(7,("winbindd_sid2gid_async: Resolving %s to a gid\n",
		sid_string_tos(sid)));

	r = TALLOC_P(mem_ctx, struct winbind_get_idmap);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_get_idmap_level);
	if (!r->in.level) goto nomem;

	*r->in.level	= WINBIND_IDMAP_LEVEL_SID_TO_GID;
	r->in.req.sid	= discard_const(sid);

	do_async_ndr(mem_ctx, idmap_child(),
		     NDR_WINBIND_GET_IDMAP, r,
		     winbindd_sid2gid_recv, r,
		     (void *)cont, private_data);
	return;
nomem:
	TALLOC_FREE(r);
	cont(private_data, false, (gid_t)-1);
	return;
}

static void winbindd_uid2sid_recv(TALLOC_CTX *mem_ctx, bool success,
				    struct winbindd_ndr_call *c,
				    void *_r,
				    void *_cont,
				    void *private_data)
{
	void (*cont)(void *priv, bool succ, const char *sid) =
		(void (*)(void *, bool, const char *))_cont;
	struct winbind_get_idmap *r =
		talloc_get_type_abort(_r, struct winbind_get_idmap);
	const char *sid;

	if (!success) {
		DEBUG(5, ("Could not get_idmap(uid2sid)\n"));
		TALLOC_FREE(r);
		cont(private_data, false, NULL);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("get_idmap(uid2sid) returned an error:0x%08X\n",
			r->out.result));
		TALLOC_FREE(r);
		cont(private_data, false, NULL);
		return;
	}

	sid = dom_sid_string(mem_ctx, r->out.rep->sid);
	if (!sid) {
		TALLOC_FREE(r);
		cont(private_data, false, NULL);
		return;
	}

	TALLOC_FREE(r);
	cont(private_data, true, sid);
}

void winbindd_uid2sid_async(TALLOC_CTX *mem_ctx, uid_t uid,
			    void (*cont)(void *private_data, bool success, const char *sid),
			    void *private_data)
{
	struct winbind_get_idmap *r = NULL;

	DEBUG(7,("winbindd_uid2sid_async: Resolving %u to a sid\n",
		uid));

	r = TALLOC_P(mem_ctx, struct winbind_get_idmap);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_get_idmap_level);
	if (!r->in.level) goto nomem;

	*r->in.level	= WINBIND_IDMAP_LEVEL_UID_TO_SID;
	r->in.req.uid	= uid;

	do_async_ndr(mem_ctx, idmap_child(),
		     NDR_WINBIND_GET_IDMAP, r,
		     winbindd_uid2sid_recv, r,
		     (void *)cont, private_data);
	return;
nomem:
	TALLOC_FREE(r);
	cont(private_data, false, NULL);
	return;
}

static void winbindd_gid2sid_recv(TALLOC_CTX *mem_ctx, bool success,
				  struct winbindd_ndr_call *c,
				  void *_r,
				  void *_cont,
				  void *private_data)
{
	void (*cont)(void *priv, bool succ, const char *sid) =
		(void (*)(void *, bool, const char *))_cont;
	struct winbind_get_idmap *r =
		talloc_get_type_abort(_r, struct winbind_get_idmap);
	const char *sid;

	if (!success) {
		DEBUG(5, ("Could not get_idmap(gid2sid)\n"));
		TALLOC_FREE(r);
		cont(private_data, false, NULL);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("get_idmap(gid2sid) returned an error:0x%08X\n",
			r->out.result));
		TALLOC_FREE(r);
		cont(private_data, false, NULL);
		return;
	}

	sid = dom_sid_string(mem_ctx, r->out.rep->sid);
	if (!sid) {
		TALLOC_FREE(r);
		cont(private_data, false, NULL);
		return;
	}

	TALLOC_FREE(r);
	cont(private_data, true, sid);
}

void winbindd_gid2sid_async(TALLOC_CTX *mem_ctx, gid_t gid,
			    void (*cont)(void *private_data, bool success, const char *sid),
			    void *private_data)
{
	struct winbind_get_idmap *r = NULL;

	DEBUG(7,("winbindd_gid2sid_async: Resolving %u to a sid\n",
		gid));

	r = TALLOC_P(mem_ctx, struct winbind_get_idmap);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_get_idmap_level);
	if (!r->in.level) goto nomem;

	*r->in.level	= WINBIND_IDMAP_LEVEL_GID_TO_SID;
	r->in.req.gid	= gid;

	do_async_ndr(mem_ctx, idmap_child(),
		     NDR_WINBIND_GET_IDMAP, r,
		     winbindd_gid2sid_recv, r,
		     (void *)cont, private_data);
	return;
nomem:
	TALLOC_FREE(r);
	cont(private_data, false, NULL);
	return;
}

static const struct winbindd_child_dispatch_table idmap_dispatch_table[] = {
	{
		.name		= "NDR_WINBIND_PING",
		.ndr_opnum	= NDR_WINBIND_PING,
		.ndr_fn		= winbindd_ndr_child_ping,
	},{
		.name		= "NDR_WINBIND_GET_IDMAP",
		.ndr_opnum	= NDR_WINBIND_GET_IDMAP,
		.ndr_fn		= winbindd_ndr_child_get_idmap,
	},{
		.name		= "NDR_WINBIND_SET_IDMAP",
		.ndr_opnum	= NDR_WINBIND_SET_IDMAP,
		.ndr_fn		= winbindd_ndr_child_set_idmap,
	},{
		.name		= NULL,
	}
};
