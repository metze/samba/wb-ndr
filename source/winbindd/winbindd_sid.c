/* 
   Unix SMB/CIFS implementation.

   Winbind daemon - sid related functions

   Copyright (C) Tim Potter 2000
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "winbindd.h"

#undef DBGC_CLASS
#define DBGC_CLASS DBGC_WINBIND

/* Convert a string  */

static void lookupsid_recv(void *private_data, bool success,
			   const char *dom_name, const char *name,
			   enum lsa_SidType type);

void winbindd_lookupsid(struct winbindd_cli_state *state)
{
	DOM_SID sid;

	/* Ensure null termination */
	state->request.data.sid[sizeof(state->request.data.sid)-1]='\0';

	DEBUG(3, ("[%5lu]: lookupsid %s\n", (unsigned long)state->pid, 
		  state->request.data.sid));

	if (!string_to_sid(&sid, state->request.data.sid)) {
		DEBUG(5, ("%s not a SID\n", state->request.data.sid));
		request_error(state);
		return;
	}

	winbindd_lookupsid_async(state->mem_ctx, &sid, lookupsid_recv, state);
}

static void lookupsid_recv(void *private_data, bool success,
			   const char *dom_name, const char *name,
			   enum lsa_SidType type)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);

	if (!success) {
		DEBUG(5, ("lookupsid returned an error\n"));
		request_error(state);
		return;
	}

	fstrcpy(state->response.data.name.dom_name, dom_name);
	fstrcpy(state->response.data.name.name, name);
	state->response.data.name.type = type;
	request_ok(state);
}

/**
 * Look up the SID for a qualified name.  
 **/

static void lookupname_recv(void *private_data, bool success,
			    const DOM_SID *sid, enum lsa_SidType type);

void winbindd_lookupname(struct winbindd_cli_state *state)
{
	char *name_domain, *name_user;
	char *p;

	/* Ensure null termination */
	state->request.data.name.dom_name[sizeof(state->request.data.name.dom_name)-1]='\0';

	/* Ensure null termination */
	state->request.data.name.name[sizeof(state->request.data.name.name)-1]='\0';

	/* cope with the name being a fully qualified name */
	p = strstr(state->request.data.name.name, lp_winbind_separator());
	if (p) {
		*p = 0;
		name_domain = state->request.data.name.name;
		name_user = p+1;
	} else {
		name_domain = state->request.data.name.dom_name;
		name_user = state->request.data.name.name;
	}

	DEBUG(3, ("[%5lu]: lookupname %s%s%s\n", (unsigned long)state->pid,
		  name_domain, lp_winbind_separator(), name_user));

	winbindd_lookupname_async(state->mem_ctx, name_domain, name_user,
				  lookupname_recv, WINBINDD_LOOKUPNAME, 
				  state);
}

static void lookupname_recv(void *private_data, bool success,
			    const DOM_SID *sid, enum lsa_SidType type)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);

	if (!success) {
		DEBUG(5, ("lookupname returned an error\n"));
		request_error(state);
		return;
	}

	sid_to_fstring(state->response.data.sid.sid, sid);
	state->response.data.sid.type = type;
	request_ok(state);
	return;
}

static void lookuprids_recv(void *private_data,
			    bool success,
			    struct winbind_lookup *r)
{
	uint32_t i;
	size_t buflen = 0;
	ssize_t len = 0;
	char *result = NULL;
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);

	if (!success) {
		request_error(state);
		return;
	}

	if (r->in.req.rids.num_rids != r->out.rep->name_array.num_names) {
		request_error(state);
		return;
	}

	for (i=0; i < r->out.rep->name_array.num_names; i++) {
		sprintf_append(state->mem_ctx, &result, &len, &buflen,
			       "%d %s\n",
			       r->out.rep->name_array.names[i].type,
			       r->out.rep->name_array.names[i].account_name);
	}

	if (r->out.rep->name_array.num_names > 0) {
		fstrcpy(state->response.data.domain_name, r->out.rep->name_array.names[0].domain_name);
	}

	state->response.extra_data.data = SMB_STRDUP(result);
	if (!state->response.extra_data.data) {
		request_error(state);
		return;
	}
	state->response.length += len+1;

	request_ok(state);
}

void winbindd_lookuprids(struct winbindd_cli_state *state)
{
	struct winbindd_domain *domain;
	struct winbind_lookup *r;

	/* Ensure null termination */
	state->request.data.sid[sizeof(state->request.data.sid)-1]='\0';

	DEBUG(10, ("lookup_rids: %s\n", state->request.data.sid));

	r = TALLOC_P(state->mem_ctx, struct winbind_lookup);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_lookup_level);
	if (!r->in.level) goto nomem;

	*r->in.level			= WINBIND_LOOKUP_LEVEL_RIDS2NAMES;
	r->in.req.rids.domain_sid	= talloc_zero(r, struct dom_sid);
	if (!r->in.req.rids.domain_sid) goto nomem;
	r->in.req.rids.rids		= NULL;
	r->in.req.rids.num_rids		= 0;

	if (!parse_ridlist(state->mem_ctx, state->request.extra_data.data,
			   &r->in.req.rids.rids, &r->in.req.rids.num_rids)) {
		DEBUG(5, ("Could not parse ridlist\n"));
		request_error(state);
		return;
	}

	if (!string_to_sid(r->in.req.rids.domain_sid, state->request.data.sid)) {
		DEBUG(5, ("Could not convert %s to SID\n",
			  state->request.data.sid));
		request_error(state);
		return;
	}

	domain = find_lookup_domain_from_sid(r->in.req.rids.domain_sid);
	if (domain == NULL) {
		DEBUG(10, ("Could not find domain for name %s\n",
			   state->request.domain_name));
		request_error(state);
		return;
	}

	winbindd_lookup_async(state->mem_ctx, domain,
			      r, lookuprids_recv, state);
	return;
nomem:
	request_error(state);
	return;
}

/* Convert a sid to a uid.  We assume we only have one rid attached to the
   sid. */

static void sid2uid_recv(void *private_data, bool success, uid_t uid)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);

	if (!success) {
		DEBUG(5, ("Could not convert sid %s\n",
			  state->request.data.sid));
		request_error(state);
		return;
	}

	state->response.data.uid = uid;
	request_ok(state);
}

static void sid2uid_lookupsid_recv( void *private_data, bool success, 
				    const char *domain_name, 
				    const char *name, 
				    enum lsa_SidType type)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);
	DOM_SID sid;

	if (!success) {
		DEBUG(5, ("sid2uid_lookupsid_recv Could not convert get sid type for %s\n",
			  state->request.data.sid));
		request_error(state);
		return;
	}

	if ( (type!=SID_NAME_USER) && (type!=SID_NAME_COMPUTER) ) {
		DEBUG(5,("sid2uid_lookupsid_recv: Sid %s is not a user or a computer.\n", 
			 state->request.data.sid));
		request_error(state);
		return;		
	}

	if (!string_to_sid(&sid, state->request.data.sid)) {
		DEBUG(1, ("sid2uid_lookupsid_recv: Could not get convert sid %s from string\n",
			  state->request.data.sid));
		request_error(state);
		return;
	}
	
	/* always use the async interface (may block) */
	winbindd_sid2uid_async(state->mem_ctx, &sid, sid2uid_recv, state);
}

void winbindd_sid_to_uid(struct winbindd_cli_state *state)
{
	DOM_SID sid;

	/* Ensure null termination */
	state->request.data.sid[sizeof(state->request.data.sid)-1]='\0';

	DEBUG(3, ("[%5lu]: sid to uid %s\n", (unsigned long)state->pid,
		  state->request.data.sid));

	if (!string_to_sid(&sid, state->request.data.sid)) {
		DEBUG(1, ("Could not get convert sid %s from string\n",
			  state->request.data.sid));
		request_error(state);
		return;
	}

	/* Validate the SID as a user.  Hopefully this will hit cache.
	   Needed to prevent DoS by exhausting the uid allocation
	   range from random SIDs. */

	winbindd_lookupsid_async( state->mem_ctx, &sid, sid2uid_lookupsid_recv, state );
}

/* Convert a sid to a gid.  We assume we only have one rid attached to the
   sid.*/

static void sid2gid_recv(void *private_data, bool success, gid_t gid)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);

	if (!success) {
		DEBUG(5, ("Could not convert sid %s\n",
			  state->request.data.sid));
		request_error(state);
		return;
	}

	state->response.data.gid = gid;
	request_ok(state);
}

static void sid2gid_lookupsid_recv( void *private_data, bool success, 
				    const char *domain_name, 
				    const char *name, 
				    enum lsa_SidType type)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);
	DOM_SID sid;

	if (!success) {
		DEBUG(5, ("sid2gid_lookupsid_recv: Could not get sid type for %s\n",
			  state->request.data.sid));
		request_error(state);
		return;
	}

	if ( (type!=SID_NAME_DOM_GRP) &&
	     (type!=SID_NAME_ALIAS) && 
	     (type!=SID_NAME_WKN_GRP) ) 
	{
		DEBUG(5,("sid2gid_lookupsid_recv: Sid %s is not a group.\n", 
			 state->request.data.sid));
		request_error(state);
		return;		
	}

	if (!string_to_sid(&sid, state->request.data.sid)) {
		DEBUG(1, ("sid2gid_lookupsid_recv: Could not get convert sid %s from string\n",
			  state->request.data.sid));
		request_error(state);
		return;
	}
	
	/* always use the async interface (may block) */
	winbindd_sid2gid_async(state->mem_ctx, &sid, sid2gid_recv, state);
}

void winbindd_sid_to_gid(struct winbindd_cli_state *state)
{
	DOM_SID sid;

	/* Ensure null termination */
	state->request.data.sid[sizeof(state->request.data.sid)-1]='\0';

	DEBUG(3, ("[%5lu]: sid to gid %s\n", (unsigned long)state->pid,
		  state->request.data.sid));

	if (!string_to_sid(&sid, state->request.data.sid)) {
		DEBUG(1, ("Could not get convert sid %s from string\n",
			  state->request.data.sid));
		request_error(state);
		return;
	}

	/* Validate the SID as a group.  Hopefully this will hit cache.
	   Needed to prevent DoS by exhausting the uid allocation
	   range from random SIDs. */

	winbindd_lookupsid_async( state->mem_ctx, &sid, sid2gid_lookupsid_recv, state );	
}

static void winbindd_set_mapping_recv(TALLOC_CTX *mem_ctx, bool success,
				      struct winbindd_ndr_call *c,
				      void *private_data,
				      void *_cont,
				      void *_cont_private)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);
	struct winbind_set_idmap *r =
		talloc_get_type_abort(c->ndr.r, struct winbind_set_idmap);

	if (!success) {
		DEBUG(5, ("Could not set_idmap(set_mapping)\n"));
		request_error(state);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("set_idmap(set_mapping) returned an error:0x%08X\n",
			r->out.result));
		request_error(state);
		return;
	}

	request_ok(state);
}

void winbindd_set_mapping(struct winbindd_cli_state *state)
{
	struct winbind_set_idmap *r = NULL;

	DEBUG(3, ("[%5lu]: set id map\n", (unsigned long)state->pid));

	if ( ! state->privileged) {
		DEBUG(0, ("Only root is allowed to set mappings!\n"));
		request_error(state);
		return;
	}

	r = TALLOC_P(state->mem_ctx, struct winbind_set_idmap);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_set_idmap_level);
	if (!r->in.level) goto nomem;

	*r->in.level = WINBIND_SET_IDMAP_LEVEL_SET_MAPPING;
	r->in.req.mapping.sid = string_sid_talloc(r,
					state->request.data.dual_idmapset.sid);
	if (!r->in.req.mapping.sid) {
		DEBUG(1, ("Could not get convert sid %s from string\n",
			  state->request.data.dual_idmapset.sid));
		goto nomem;
	}
	r->in.req.mapping.xid.id = state->request.data.dual_idmapset.id;
	r->in.req.mapping.xid.type = state->request.data.dual_idmapset.type;
	r->in.req.mapping.status = ID_MAPPED;

	do_async_ndr(state->mem_ctx, idmap_child(),
		     NDR_WINBIND_SET_IDMAP, r,
		     winbindd_set_mapping_recv, state,
		     NULL, NULL);
	return;
nomem:
	request_error(state);
	return;
}

static void winbindd_set_hwm_recv(TALLOC_CTX *mem_ctx, bool success,
				  struct winbindd_ndr_call *c,
				  void *private_data,
				  void *_cont,
				  void *_cont_private)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);
	struct winbind_set_idmap *r =
		talloc_get_type_abort(c->ndr.r, struct winbind_set_idmap);

	if (!success) {
		DEBUG(5, ("Could not set_idmap(set_hwm)\n"));
		request_error(state);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("set_idmap(set_hwm) returned an error:0x%08X\n",
			r->out.result));
		request_error(state);
		return;
	}

	request_ok(state);
}

void winbindd_set_hwm(struct winbindd_cli_state *state)
{
	struct winbind_set_idmap *r = NULL;

	DEBUG(3, ("[%5lu]: set hwm\n", (unsigned long)state->pid));

	if ( ! state->privileged) {
		DEBUG(0, ("Only root is allowed to set mappings!\n"));
		request_error(state);
		return;
	}

	r = TALLOC_P(state->mem_ctx, struct winbind_set_idmap);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_set_idmap_level);
	if (!r->in.level) goto nomem;

	*r->in.level = WINBIND_SET_IDMAP_LEVEL_SET_HWM;
	r->in.req.hwm.id = state->request.data.dual_idmapset.id;
	r->in.req.hwm.type = state->request.data.dual_idmapset.type;

	do_async_ndr(state->mem_ctx, idmap_child(),
		     NDR_WINBIND_SET_IDMAP, r,
		     winbindd_set_hwm_recv, state,
		     NULL, NULL);
	return;
nomem:
	request_error(state);
	return;
}

/* Convert a uid to a sid */

static void uid2sid_recv(void *private_data, bool success, const char *sid)
{
	struct winbindd_cli_state *state =
		(struct winbindd_cli_state *)private_data;

	if (success) {
		DEBUG(10,("uid2sid: uid %lu has sid %s\n",
			  (unsigned long)(state->request.data.uid), sid));
		fstrcpy(state->response.data.sid.sid, sid);
		state->response.data.sid.type = SID_NAME_USER;
		request_ok(state);
		return;
	}

	request_error(state);
	return;
}

void winbindd_uid_to_sid(struct winbindd_cli_state *state)
{
	DEBUG(3, ("[%5lu]: uid to sid %lu\n", (unsigned long)state->pid, 
		  (unsigned long)state->request.data.uid));

	/* always go via the async interface (may block) */
	winbindd_uid2sid_async(state->mem_ctx, state->request.data.uid, uid2sid_recv, state);
}

/* Convert a gid to a sid */

static void gid2sid_recv(void *private_data, bool success, const char *sid)
{
	struct winbindd_cli_state *state =
		(struct winbindd_cli_state *)private_data;

	if (success) {
		DEBUG(10,("gid2sid: gid %lu has sid %s\n",
			  (unsigned long)(state->request.data.gid), sid));
		fstrcpy(state->response.data.sid.sid, sid);
		state->response.data.sid.type = SID_NAME_DOM_GRP;
		request_ok(state);
		return;
	}

	request_error(state);
	return;
}


void winbindd_gid_to_sid(struct winbindd_cli_state *state)
{
	DEBUG(3, ("[%5lu]: gid to sid %lu\n", (unsigned long)state->pid, 
		  (unsigned long)state->request.data.gid));

	/* always use async calls (may block) */
	winbindd_gid2sid_async(state->mem_ctx, state->request.data.gid, gid2sid_recv, state);
}

static void winbindd_allocate_uid_recv(TALLOC_CTX *mem_ctx, bool success,
				       struct winbindd_ndr_call *c,
				       void *private_data,
				       void *_cont,
				       void *_cont_private)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);
	struct winbind_set_idmap *r =
		talloc_get_type_abort(c->ndr.r, struct winbind_set_idmap);

	if (!success) {
		DEBUG(5, ("Could not set_idmap(allocate_uid)\n"));
		request_error(state);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("set_idmap(allocate_uid) returned an error:0x%08X\n",
			r->out.result));
		request_error(state);
		return;
	}

	if (r->out.rep->uid > UINT32_MAX) {
		DEBUG(1, ("set_idmap(allocate_uid) returned a 64bit uid %llu\n",
			(unsigned long long)r->out.rep->uid));
		request_error(state);
		return;
	}

	state->response.data.uid = r->out.rep->uid;
	request_ok(state);
}

void winbindd_allocate_uid(struct winbindd_cli_state *state)
{
	struct winbind_set_idmap *r = NULL;

	if ( !state->privileged ) {
		DEBUG(2, ("winbindd_allocate_uid: non-privileged access "
			  "denied!\n"));
		request_error(state);
		return;
	}

	r = TALLOC_P(state->mem_ctx, struct winbind_set_idmap);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_set_idmap_level);
	if (!r->in.level) goto nomem;

	*r->in.level = WINBIND_SET_IDMAP_LEVEL_ALLOCATE_UID;

	do_async_ndr(state->mem_ctx, idmap_child(),
		     NDR_WINBIND_SET_IDMAP, r,
		     winbindd_allocate_uid_recv, state,
		     NULL, NULL);
	return;
nomem:
	request_error(state);
	return;
}

static void winbindd_allocate_gid_recv(TALLOC_CTX *mem_ctx, bool success,
				       struct winbindd_ndr_call *c,
				       void *private_data,
				       void *_cont,
				       void *_cont_private)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);
	struct winbind_set_idmap *r =
		talloc_get_type_abort(c->ndr.r, struct winbind_set_idmap);

	if (!success) {
		DEBUG(5, ("Could not set_idmap(allocate_gid)\n"));
		request_error(state);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("set_idmap(allocate_gid) returned an error:0x%08X\n",
			r->out.result));
		request_error(state);
		return;
	}

	if (r->out.rep->gid > UINT32_MAX) {
		DEBUG(1, ("set_idmap(allocate_gid) returned a 64bit gid %llu\n",
			(unsigned long long)r->out.rep->gid));
		request_error(state);
		return;
	}

	state->response.data.gid = r->out.rep->gid;
	request_ok(state);
}

void winbindd_allocate_gid(struct winbindd_cli_state *state)
{
	struct winbind_set_idmap *r = NULL;

	if ( !state->privileged ) {
		DEBUG(2, ("winbindd_allocate_gid: non-privileged access "
			  "denied!\n"));
		request_error(state);
		return;
	}

	r = TALLOC_P(state->mem_ctx, struct winbind_set_idmap);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_set_idmap_level);
	if (!r->in.level) goto nomem;

	*r->in.level = WINBIND_SET_IDMAP_LEVEL_ALLOCATE_GID;

	do_async_ndr(state->mem_ctx, idmap_child(),
		     NDR_WINBIND_SET_IDMAP, r,
		     winbindd_allocate_gid_recv, state,
		     NULL, NULL);
	return;
nomem:
	request_error(state);
	return;
}
