/* 
   Unix SMB/CIFS implementation.

   Async helpers for blocking functions

   Copyright (C) Volker Lendecke 2005
   Copyright (C) Gerald Carter 2006
   
   The helpers always consist of three functions: 

   * A request setup function that takes the necessary parameters together
     with a continuation function that is to be called upon completion

   * A private continuation function that is internal only. This is to be
     called by the lower-level functions in do_async(). Its only task is to
     properly call the continuation function named above.

   * A worker function that is called inside the appropriate child process.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "winbindd.h"

#undef DBGC_CLASS
#define DBGC_CLASS DBGC_WINBIND

struct do_async_state {
	TALLOC_CTX *mem_ctx;
	struct winbindd_request request;
	struct winbindd_response response;
	void (*cont)(TALLOC_CTX *mem_ctx,
		     bool success,
		     struct winbindd_response *response,
		     void *c, void *private_data);
	void *c, *private_data;
};

static void do_async_recv(void *private_data, bool success)
{
	struct do_async_state *state =
		talloc_get_type_abort(private_data, struct do_async_state);

	state->cont(state->mem_ctx, success, &state->response,
		    state->c, state->private_data);
}

void do_async(TALLOC_CTX *mem_ctx, struct winbindd_child *child,
	      const struct winbindd_request *request,
	      void (*cont)(TALLOC_CTX *mem_ctx, bool success,
			   struct winbindd_response *response,
			   void *c, void *private_data),
	      void *c, void *private_data)
{
	struct do_async_state *state;

	state = TALLOC_ZERO_P(mem_ctx, struct do_async_state);
	if (state == NULL) {
		DEBUG(0, ("talloc failed\n"));
		cont(mem_ctx, False, NULL, c, private_data);
		return;
	}

	state->mem_ctx = mem_ctx;
	state->request = *request;
	state->request.length = sizeof(state->request);
	state->cont = cont;
	state->c = c;
	state->private_data = private_data;

	async_request(mem_ctx, child, &state->request,
		      &state->response, do_async_recv, state);
}

void do_async_domain(TALLOC_CTX *mem_ctx, struct winbindd_domain *domain,
		     const struct winbindd_request *request,
		     void (*cont)(TALLOC_CTX *mem_ctx, bool success,
				  struct winbindd_response *response,
				  void *c, void *private_data),
		     void *c, void *private_data)
{
	struct do_async_state *state;

	state = TALLOC_ZERO_P(mem_ctx, struct do_async_state);
	if (state == NULL) {
		DEBUG(0, ("talloc failed\n"));
		cont(mem_ctx, False, NULL, c, private_data);
		return;
	}

	state->mem_ctx = mem_ctx;
	state->request = *request;
	state->request.length = sizeof(state->request);
	state->cont = cont;
	state->c = c;
	state->private_data = private_data;

	async_domain_request(mem_ctx, domain, &state->request,
			     &state->response, do_async_recv, state);
}

struct do_async_ndr_state {
	TALLOC_CTX *mem_ctx;
	struct winbindd_ndr_call call;
	void (*cont)(TALLOC_CTX *mem_ctx, bool success,
		     struct winbindd_ndr_call *call,
		     void *private_data,
		     void *caller_cont,
		     void *caller_private);
	void *private_data;
	void *caller_cont;
	void *caller_private;
};

static void do_async_ndr_recv(void *private_data, bool success)
{
	struct do_async_ndr_state *state =
		talloc_get_type_abort(private_data, struct do_async_ndr_state);

	state->cont(state->mem_ctx, success,
		    &state->call, state->private_data,
		    state->caller_cont, state->caller_private);
}

void do_async_ndr(TALLOC_CTX *mem_ctx, struct winbindd_child *child,
		  uint32 opnum, void *r,
		  void (*cont)(TALLOC_CTX *mem_ctx, bool success,
			       struct winbindd_ndr_call *call,
			       void *private_data,
			       void *caller_cont,
			       void *caller_private),
		  void *private_data,
		  void *caller_cont,
		  void *caller_private)
{
	struct do_async_ndr_state *state;

	SMB_ASSERT(opnum < ndr_table_winbind_protocol.num_calls);

	state = TALLOC_ZERO_P(mem_ctx, struct do_async_ndr_state);
	if (state == NULL) {
		DEBUG(0, ("talloc failed\n"));
		cont(mem_ctx, False, NULL, private_data,
		     caller_cont, caller_private);
		return;
	}

	state->mem_ctx = mem_ctx;
	state->call.ndr.call = &ndr_table_winbind_protocol.calls[opnum];
	state->call.ndr.r = r;
	state->cont = cont;
	state->private_data = private_data;
	state->caller_cont = caller_cont;
	state->caller_private = caller_private;

	async_ndr_call(mem_ctx, child, &state->call,
		       do_async_ndr_recv, state);
}

void do_async_ndr_domain(TALLOC_CTX *mem_ctx, struct winbindd_domain *domain,
			 uint32 opnum, void *r,
			 void (*cont)(TALLOC_CTX *mem_ctx, bool success,
				      struct winbindd_ndr_call *call,
				      void *private_data,
				      void *caller_cont,
				      void *caller_private),
			 void *private_data,
			 void *caller_cont,
			 void *caller_private)
{
	struct do_async_ndr_state *state;

	SMB_ASSERT(opnum < ndr_table_winbind_protocol.num_calls);

	state = TALLOC_ZERO_P(mem_ctx, struct do_async_ndr_state);
	if (state == NULL) {
		DEBUG(0, ("talloc failed\n"));
		cont(mem_ctx, False, NULL, private_data,
		     caller_cont, caller_private);
		return;
	}

	state->mem_ctx = mem_ctx;
	state->call.ndr.call = &ndr_table_winbind_protocol.calls[opnum];
	state->call.ndr.r = r;
	state->cont = cont;
	state->private_data = private_data;
	state->caller_cont = caller_cont;
	state->caller_private = caller_private;

	async_ndr_domain_call(mem_ctx, domain, &state->call,
			      do_async_ndr_recv, state);
}

static void winbindd_lookupsid_recv2(TALLOC_CTX *mem_ctx, bool success,
				     struct winbindd_ndr_call *c,
				     void *_private_data,
				     void *_cont,
				     void *cont_private)
{
	void (*cont)(void *priv, bool succ, const char *dom_name,
		     const char *name, enum lsa_SidType type) =
		(void (*)(void *, bool, const char *, const char *,
			  enum lsa_SidType))_cont;
	struct winbind_lookup *r =
		talloc_get_type_abort(_private_data, struct winbind_lookup);

	if (!success) {
		DEBUG(5, ("Could not trigger lookup(sid2name)\n"));
		TALLOC_FREE(r);
		cont(cont_private, false, NULL, NULL, SID_NAME_UNKNOWN);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5,("lookup(sid2name) returned an error:0x%08X\n"
			 " (root domain)\n", r->out.result));
		TALLOC_FREE(r);
		cont(cont_private, false, NULL, NULL, SID_NAME_UNKNOWN);
		return;
	}

	cont(cont_private, true,
	     r->out.rep->name_info.domain_name,
	     r->out.rep->name_info.account_name,
	     r->out.rep->name_info.type);
}

static void winbindd_lookupsid_recv1(TALLOC_CTX *mem_ctx, bool success,
				     struct winbindd_ndr_call *c,
				     void *_private_data,
				     void *_cont,
				     void *cont_private)
{
	void (*cont)(void *priv, bool succ, const char *dom_name,
		     const char *name, enum lsa_SidType type) =
		(void (*)(void *, bool, const char *, const char *,
			  enum lsa_SidType))_cont;
	struct winbind_lookup *r =
		talloc_get_type_abort(_private_data, struct winbind_lookup);

	if (!success) {
		DEBUG(5, ("Could not trigger lookup(sid2name)\n"));
		TALLOC_FREE(r);
		cont(cont_private, false, NULL, NULL, SID_NAME_UNKNOWN);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		struct winbindd_domain *root_domain = find_root_domain();

		if ( !root_domain ) {
			DEBUG(5,("lookup(sid2name) returned an error:0x%08X\n"
				 " (no root domain as fallback)\n", r->out.result));

			TALLOC_FREE(r);
			cont(cont_private, false, NULL, NULL, SID_NAME_UNKNOWN);
			return;
		}
		DEBUG(5,("lookup(sid2name) returned an error:0x%08X"
			 " (fallback to root domain)\n", r->out.result));

		do_async_ndr_domain(mem_ctx, root_domain,
				    NDR_WINBIND_LOOKUP, r,
				    winbindd_lookupsid_recv2, r,
				    (void *)cont, cont_private);
		return;
	}

	cont(cont_private, true,
	     r->out.rep->name_info.domain_name,
	     r->out.rep->name_info.account_name,
	     r->out.rep->name_info.type);
}

void winbindd_lookupsid_async(TALLOC_CTX *mem_ctx, const DOM_SID *sid,
			      void (*cont)(void *private_data, bool success,
					   const char *dom_name,
					   const char *name,
					   enum lsa_SidType type),
			      void *cont_private)
{
	struct winbindd_domain *domain;
	struct winbind_lookup *r = NULL;

	domain = find_lookup_domain_from_sid(sid);
	if (domain == NULL) {
		DEBUG(5, ("Could not find domain for sid %s\n",
			  sid_string_dbg(sid)));
		cont(cont_private, false, NULL, NULL, SID_NAME_UNKNOWN);
		return;
	}

	r = TALLOC_P(mem_ctx, struct winbind_lookup);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_lookup_level);
	if (!r->in.level) goto nomem;

	*r->in.level	= WINBIND_LOOKUP_LEVEL_SID2NAME;
	r->in.req.sid	= sid_dup_talloc(r, sid);
	if (!r->in.req.sid) goto nomem;

	do_async_ndr_domain(mem_ctx, domain,
			    NDR_WINBIND_LOOKUP, r,
			    winbindd_lookupsid_recv1, r,
			    (void *)cont, cont_private);
	return;
nomem:
	TALLOC_FREE(r);
	cont(cont_private, false, NULL, NULL, SID_NAME_UNKNOWN);
	return;
}

static void ndr_child_lookup_sid2name(struct winbindd_domain *domain,
				      struct winbindd_cli_state *state,
				      struct winbind_lookup *r)
{
	bool ok;

	DEBUG(3, ("lookup sid2name %s\n",
		  sid_string_dbg(r->in.req.sid)));

	/* Lookup the sid */

	ok = winbindd_lookup_name_by_sid(r, domain,
					 r->in.req.sid,
					 (char **)&r->out.rep->name_info.domain_name,
					 (char **)&r->out.rep->name_info.account_name,
					 &r->out.rep->name_info.type);
	if (!ok) {
		DEBUG(1, ("Can't lookup name by sid\n"));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	r->out.result = WINBIND_STATUS_OK;
}

static void ndr_child_lookup_name2sid(struct winbindd_domain *domain,
				      struct winbindd_cli_state *state,
				      struct winbind_lookup *r)
{
	bool ok;
	char *name_domain;
	char *name_user;
	char *p;

	DEBUG(3, ("lookup name2sid %s\n",
		  r->in.req.name));

	name_domain = talloc_strdup(r, r->in.req.name);
	if (!name_domain) {
		r->out.result = WINBIND_STATUS_NO_MEMORY;
		return;
	}

	/* the name must be a fully qualified name */
	p = strstr(name_domain, lp_winbind_separator());
	if (!p) {
		r->out.result = WINBIND_STATUS_INVALID_PARAMETER;
		return;
	}

	*p = 0;
	name_user = p+1;

	r->out.rep->sid_info.sid = TALLOC_ZERO_P(r, struct dom_sid);
	if (!r->out.rep->sid_info.sid) {
		r->out.result = WINBIND_STATUS_NO_MEMORY;
		return;
	}

	/* Lookup name from DC using lsa_lookup_names() */
/* TODO: */	ok = winbindd_lookup_sid_by_name(state->mem_ctx, WINBINDD_LOOKUPNAME,
					 domain, name_domain, name_user,
					 r->out.rep->sid_info.sid,
					 &r->out.rep->sid_info.type);
	if (!ok) {
		DEBUG(1, ("Can't lookup name by sid\n"));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	r->out.result = WINBIND_STATUS_OK;
}

static void ndr_child_lookup_rids2names(struct winbindd_domain *domain,
				        struct winbindd_cli_state *state,
				        struct winbind_lookup *r)
{
	char *domain_name;
	char **names;
	enum lsa_SidType *types;
	NTSTATUS status;
	uint32_t i;
	struct winbind_lookup_name_info *n;

	DEBUG(3, ("lookup rids2name domain:%s  num %u\n",
		  sid_string_dbg(r->in.req.rids.domain_sid),
		  r->in.req.rids.num_rids));


	status = domain->methods->rids_to_names(domain, state->mem_ctx,
						r->in.req.rids.domain_sid,
						r->in.req.rids.rids,
						r->in.req.rids.num_rids,
						&domain_name,
						&names, &types);

	if (!NT_STATUS_IS_OK(status) &&
	    !NT_STATUS_EQUAL(status, STATUS_SOME_UNMAPPED)) {
		DEBUG(1, ("Can't lookup names by rids: %s\n",
			  nt_errstr(status)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	r->out.rep->name_array.num_names = r->in.req.rids.num_rids;
	n = talloc_array(r,
			 struct winbind_lookup_name_info,
			 r->out.rep->name_array.num_names);
	if (!n) {
		r->out.result = WINBIND_STATUS_NO_MEMORY;
		return;
	}
	r->out.rep->name_array.names = n;

	for (i=0; i < r->out.rep->name_array.num_names; i++) {
		n[i].domain_name	= domain_name;
		n[i].account_name	= names[i];
		n[i].type		= types[i];
	}

	r->out.result = WINBIND_STATUS_OK;
}

static void ndr_child_lookup_sid2userinfo(struct winbindd_domain *domain,
					  struct winbindd_cli_state *state,
					  struct winbind_lookup *r)
{
	NTSTATUS status;
	WINBIND_USERINFO user_info;

	DEBUG(3, ("lookup sid2userinfo user:%s\n",
		  sid_string_dbg(r->in.req.sid)));


	status = domain->methods->query_user(domain, state->mem_ctx,
					     r->in.req.sid, &user_info);
	if (!NT_STATUS_IS_OK(status)) {
		DEBUG(1, ("error getting user info for sid %s: %s\n",
			  sid_string_dbg(r->in.req.sid), nt_errstr(status)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	r->out.rep->user_info.account		= user_info.acct_name;
	r->out.rep->user_info.gecos		= user_info.full_name;
	r->out.rep->user_info.homedir		= user_info.homedir;
	r->out.rep->user_info.shell		= user_info.shell;
	r->out.rep->user_info.primary_gid	= user_info.primary_gid;

	if (!sid_peek_check_rid(&domain->sid, &user_info.group_sid,
				&r->out.rep->user_info.primary_rid)) {
		DEBUG(1, ("Could not extract group rid out of %s\n",
			  sid_string_dbg(&user_info.group_sid)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	r->out.result = WINBIND_STATUS_OK;
}

static void ndr_child_lookup_sid2domgroups(struct winbindd_domain *domain,
					   struct winbindd_cli_state *state,
					   struct winbind_lookup *r)
{
	NTSTATUS status;
	struct winbind_lookup_sid_info *a;
	struct dom_sid *groups;
	uint32_t i, num_groups;

	DEBUG(3, ("lookup sid2domgroups user:%s\n",
		  sid_string_dbg(r->in.req.sid)));

	status = domain->methods->lookup_usergroups(domain, state->mem_ctx,
						    r->in.req.sid, &num_groups,
						    &groups);
	if (!NT_STATUS_IS_OK(status)) {
		DEBUG(1, ("error getting user domain groups info for sid %s: %s\n",
			  sid_string_dbg(r->in.req.sid), nt_errstr(status)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	a = talloc_array(r, struct winbind_lookup_sid_info, num_groups);
	if (!a) {
		r->out.result = WINBIND_STATUS_NO_MEMORY;
		return;
	}

	for (i=0; i < num_groups; i++) {
		a[i].sid	= &groups[i];
		/* TODO: get this from the backend */
		a[i].type	= SID_NAME_DOM_GRP;
	}

	r->out.rep->sid_array.num_sids		= num_groups;
	r->out.rep->sid_array.sids		= a;

	r->out.result = WINBIND_STATUS_OK;
}

static void ndr_child_lookup_expandaliases(struct winbindd_domain *domain,
					   struct winbindd_cli_state *state,
					   struct winbind_lookup *r)
{
	NTSTATUS status;
	uint32_t i;
	uint32_t num_sids = 0;
	struct dom_sid *sids = NULL;
	uint32_t num_aliases = 0;
	uint32_t *alias_rids = NULL;
	struct winbind_lookup_sid_info *a;

	DEBUG(3, ("lookup expandaliases\n"));

	for (i=0; i < r->in.req.sid_array.num_sids; i++) {
		status = add_sid_to_array(state->mem_ctx,
					  r->in.req.sid_array.sids[i].sid,
					  &sids, &num_sids);
		if (!NT_STATUS_IS_OK(status)) {
			DEBUG(1, ("error copying sid %s: %s\n",
				  sid_string_dbg(r->in.req.sid_array.sids[i].sid),
				  nt_errstr(status)));
			r->out.result = WINBIND_STATUS_FOOBAR;
			return;
		}
	}

	status = domain->methods->lookup_useraliases(domain,
						     state->mem_ctx,
						     num_sids,
						     sids,
						     &num_aliases,
						     &alias_rids);
	TALLOC_FREE(sids);
	if (!NT_STATUS_IS_OK(status)) {
		DEBUG(3, ("Could not lookup_useraliases: %s\n",
			  nt_errstr(status)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	num_sids = 0;

	DEBUG(10, ("Got %d aliases\n", num_aliases));

	for (i=0; i<num_aliases; i++) {
		DOM_SID sid;
		DEBUGADD(10, (" rid %d\n", alias_rids[i]));
		sid_copy(&sid, &domain->sid);
		sid_append_rid(&sid, alias_rids[i]);
		status = add_sid_to_array(state->mem_ctx, &sid, &sids,
					  &num_sids);
		if (!NT_STATUS_IS_OK(status)) {
			DEBUG(1, ("error copying sid %s: %s\n",
				  sid_string_dbg(r->in.req.sid_array.sids[i].sid),
				  nt_errstr(status)));
			r->out.result = WINBIND_STATUS_FOOBAR;
			return;
		}
	}

	a = talloc_array(r, struct winbind_lookup_sid_info, num_aliases);
	if (!a) {
		r->out.result = WINBIND_STATUS_NO_MEMORY;
		return;
	}

	for (i=0; i < num_aliases; i++) {
		a[i].sid	= &sids[i];
		/* TODO: get this from the backend */
		a[i].type	= SID_NAME_ALIAS;
	}

	r->out.rep->sid_array.num_sids		= num_aliases;
	r->out.rep->sid_array.sids		= a;

	r->out.result = WINBIND_STATUS_OK;
}

void winbindd_ndr_domain_child_lookup(struct winbindd_domain *domain,
				      struct winbindd_cli_state *state)
{
	struct winbind_lookup *r;

	r = talloc_get_type_abort(state->c.ndr.r,
				  struct winbind_lookup);

	switch (*r->in.level) {
	case WINBIND_LOOKUP_LEVEL_SID2NAME:
		ndr_child_lookup_sid2name(domain, state, r);
		return;

	case WINBIND_LOOKUP_LEVEL_NAME2SID:
		ndr_child_lookup_name2sid(domain, state, r);
		return;

	case WINBIND_LOOKUP_LEVEL_RIDS2NAMES:
		ndr_child_lookup_rids2names(domain, state, r);
		return;

	case WINBIND_LOOKUP_LEVEL_SID2USERINFO:
		ndr_child_lookup_sid2userinfo(domain, state, r);
		return;

	case WINBIND_LOOKUP_LEVEL_SID2DOMGROUPS:
		ndr_child_lookup_sid2domgroups(domain, state, r);
		return;

	case WINBIND_LOOKUP_LEVEL_EXPANDALIASES:
		ndr_child_lookup_expandaliases(domain, state, r);
		return;
	}

	r->out.result = WINBIND_STATUS_UNKNOWN_LEVEL;
	return;
}

/********************************************************************
 This is the second callback after contacting the forest root
********************************************************************/

static void winbindd_lookupname_recv2(TALLOC_CTX *mem_ctx, bool success,
				      struct winbindd_ndr_call *c,
				      void *_private_data,
				      void *_cont,
				      void *cont_private)
{
	void (*cont)(void *priv, bool succ,
		     const DOM_SID *sid, enum lsa_SidType type) =
		(void (*)(void *, bool, const DOM_SID *,
			  enum lsa_SidType))_cont;
	struct winbind_lookup *r =
		talloc_get_type_abort(_private_data, struct winbind_lookup);

	if (!success) {
		DEBUG(5, ("Could not trigger lookup(name2sid)\n"));
		TALLOC_FREE(r);
		cont(cont_private, false, NULL, SID_NAME_UNKNOWN);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5,("lookup(name2sid) returned an error:0x%08X\n"
			 " (root domain)\n", r->out.result));
		TALLOC_FREE(r);
		cont(cont_private, false, NULL, SID_NAME_UNKNOWN);
		return;
	}

	cont(cont_private, true,
	     r->out.rep->sid_info.sid,
	     r->out.rep->sid_info.type);
}

/********************************************************************
 This is the first callback after contacting our own domain
********************************************************************/

static void winbindd_lookupname_recv1(TALLOC_CTX *mem_ctx, bool success,
				      struct winbindd_ndr_call *c,
				      void *_private_data,
				      void *_cont,
				      void *cont_private)
{
	void (*cont)(void *priv, bool succ,
		     const DOM_SID *sid, enum lsa_SidType type) =
		(void (*)(void *, bool, const DOM_SID *,
			  enum lsa_SidType))_cont;
	struct winbind_lookup *r =
		talloc_get_type_abort(_private_data, struct winbind_lookup);

	if (!success) {
		DEBUG(5, ("Could not trigger lookup(name2sid)\n"));
		TALLOC_FREE(r);
		cont(cont_private, false, NULL, SID_NAME_UNKNOWN);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		struct winbindd_domain *root_domain = find_root_domain();

		if ( !root_domain ) {
			DEBUG(5,("lookup(name2sid) returned an error:0x%08X\n"
				 " (no root domain as fallback)\n", r->out.result));

			TALLOC_FREE(r);
			cont(cont_private, false, NULL, SID_NAME_UNKNOWN);
			return;
		}
		DEBUG(5,("lookup(name2sid) returned an error:0x%08X"
			 " (fallback to root domain)\n", r->out.result));

		do_async_ndr_domain(mem_ctx, root_domain,
				    NDR_WINBIND_LOOKUP, r,
				    winbindd_lookupname_recv2, r,
				    (void *)cont, cont_private);
		return;
	}

	cont(cont_private, true,
	     r->out.rep->sid_info.sid,
	     r->out.rep->sid_info.type);
}

/********************************************************************
 The lookup name call first contacts a DC in its own domain
 and fallbacks to contact a DC in the forest in our domain doesn't
 know the name.
********************************************************************/

void winbindd_lookupname_async(TALLOC_CTX *mem_ctx,
			       const char *dom_name, const char *name,
			       void (*cont)(void *private_data, bool success,
					    const DOM_SID *sid,
					    enum lsa_SidType type),
			       enum winbindd_cmd orig_cmd,
			       void *cont_private)
{
	struct winbindd_domain *domain;
	struct winbind_lookup *r = NULL;

	if ( (domain = find_lookup_domain_from_name(dom_name)) == NULL ) {
		DEBUG(5, ("Could not find domain for name '%s'\n", dom_name));
		cont(cont_private, false, NULL, SID_NAME_UNKNOWN);
		return;
	}

	r = TALLOC_P(mem_ctx, struct winbind_lookup);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_lookup_level);
	if (!r->in.level) goto nomem;

	*r->in.level	= WINBIND_LOOKUP_LEVEL_NAME2SID;
	r->in.req.name	= talloc_asprintf(r, "%s%s%s",
					  dom_name,
					  lp_winbind_separator(),
					  name);
	if (!r->in.req.name) goto nomem;
/*TODO: pass down orig_cmd */
	do_async_ndr_domain(mem_ctx, domain,
			    NDR_WINBIND_LOOKUP, r,
			    winbindd_lookupname_recv1, r,
			    (void *)cont, cont_private);
	return;
nomem:
	TALLOC_FREE(r);
	cont(cont_private, false, NULL, SID_NAME_UNKNOWN);
	return;
}

bool parse_ridlist(TALLOC_CTX *mem_ctx, char *ridstr,
		   uint32 **rids, size_t *num_rids)
{
	char *p;

	p = ridstr;
	if (p == NULL)
		return False;

	while (p[0] != '\0') {
		uint32 rid;
		char *q;
		rid = strtoul(p, &q, 10);
		if (*q != '\n') {
			DEBUG(0, ("Got invalid ridstr: %s\n", p));
			return False;
		}
		p = q+1;
		ADD_TO_ARRAY(mem_ctx, uint32, rid, rids, num_rids);
	}
	return True;
}

static void winbindd_lookup_recv(TALLOC_CTX *mem_ctx, bool success,
				 struct winbindd_ndr_call *c,
				 void *_r,
				 void *_cont,
				 void *cont_private)
{
	void (*cont)(void *priv, bool succ, struct winbind_lookup *r) =
		(void (*)(void *, bool, struct winbind_lookup*))_cont;
	struct winbind_lookup *r =
		talloc_get_type_abort(_r, struct winbind_lookup);

	if (!success) {
		DEBUG(5, ("Could not lookup\n"));
		cont(cont_private, false, r);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("lookup returned an error:0x%08X\n",
			r->out.result));
		cont(cont_private, false, r);
		return;
	}

	cont(cont_private, true, r);
}

void winbindd_lookup_async(TALLOC_CTX *mem_ctx,
			   struct winbindd_domain *domain,
			   struct winbind_lookup *r,
			   void (*cont)(void *private_data,
					bool success,
					struct winbind_lookup *r),
			   void *cont_private)
{
	do_async_ndr_domain(mem_ctx, domain,
			    NDR_WINBIND_LOOKUP, r,
			    winbindd_lookup_recv, r,
			    (void *)cont, cont_private);
}

static void getsidaliases_recv(TALLOC_CTX *mem_ctx, bool success,
			       struct winbindd_ndr_call *c,
			       void *_private_data,
			       void *_cont,
			       void *cont_private)
{
	void (*cont)(void *priv, bool succ,
		     struct winbind_lookup_sid_info_array *aliases) =
		(void (*)(void *, bool,
			  struct winbind_lookup_sid_info_array *))_cont;
	struct winbind_lookup *r =
		talloc_get_type_abort(_private_data, struct winbind_lookup);

	if (!success) {
		TALLOC_FREE(r);
		DEBUG(5, ("Could not trigger getsidaliases\n"));
		cont(cont_private, success, NULL);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("getsidaliases returned an error: 0x%08X\n",
			r->out.result));
		TALLOC_FREE(r);
		cont(cont_private, false, NULL);
		return;
	}

	cont(cont_private, true, &r->out.rep->sid_array);
}

static void winbindd_getsidaliases_async(struct winbindd_domain *domain,
					 TALLOC_CTX *mem_ctx,
					 const DOM_SID *sids, uint32_t num_sids,
					 void (*cont)(void *private_data,
						      bool success,
						      struct winbind_lookup_sid_info_array *a),
					 void *cont_private)
{
	struct winbind_lookup *r = NULL;
	uint32_t i;

	if (num_sids == 0) {
		cont(cont_private, true, NULL);
		return;
	}

	r = TALLOC_P(mem_ctx, struct winbind_lookup);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_lookup_level);
	if (!r->in.level) goto nomem;

	*r->in.level	= WINBIND_LOOKUP_LEVEL_EXPANDALIASES;

	r->in.req.sid_array.sids = talloc_array(r,
						struct winbind_lookup_sid_info,
						num_sids);
	if (!r->in.req.sid_array.sids) goto nomem;

	for (i=0; i < num_sids; i++) {
		r->in.req.sid_array.sids[i].sid = sid_dup_talloc(r, &sids[i]);
		if (!r->in.req.sid_array.sids[i].sid) goto nomem;
		r->in.req.sid_array.sids[i].type = SID_NAME_UNKNOWN;
	}

	r->in.req.sid_array.num_sids = num_sids;

	do_async_ndr_domain(mem_ctx, domain,
			    NDR_WINBIND_LOOKUP, r,
			    getsidaliases_recv, r,
			    (void *)cont, cont_private);
	return;
nomem:
	TALLOC_FREE(r);
	cont(cont_private, false, NULL);
	return;
}

struct gettoken_state {
	TALLOC_CTX *mem_ctx;
	DOM_SID user_sid;
	struct winbindd_domain *alias_domain;
	struct winbindd_domain *local_alias_domain;
	struct winbindd_domain *builtin_domain;
	DOM_SID *sids;
	size_t num_sids;
	void (*cont)(void *private_data, bool success, DOM_SID *sids, size_t num_sids);
	void *private_data;
};

static void gettoken_recvdomgroups(TALLOC_CTX *mem_ctx, bool success,
				   struct winbindd_ndr_call *c,
				   void *_private_data,
				   void *_cont,
				   void *cont_private);
static void gettoken_recvaliases(void *private_data, bool success,
				 struct winbind_lookup_sid_info_array *aliases);

void winbindd_gettoken_async(TALLOC_CTX *mem_ctx, const DOM_SID *user_sid,
			     void (*cont)(void *private_data, bool success,
					  DOM_SID *sids, size_t num_sids),
			     void *private_data)
{
	struct winbindd_domain *domain;
	struct gettoken_state *state;
	struct winbind_lookup *r = NULL;

	state = TALLOC_ZERO_P(mem_ctx, struct gettoken_state);
	if (state == NULL) {
		DEBUG(0, ("talloc failed\n"));
		cont(private_data, False, NULL, 0);
		return;
	}

	state->mem_ctx = mem_ctx;
	sid_copy(&state->user_sid, user_sid);
	state->alias_domain = find_our_domain();
	state->local_alias_domain = find_domain_from_name( get_global_sam_name() );
	state->builtin_domain = find_builtin_domain();
	state->cont = cont;
	state->private_data = private_data;

	domain = find_domain_from_sid_noinit(user_sid);
	if (domain == NULL) {
		DEBUG(5, ("Could not find domain from SID %s\n",
			  sid_string_dbg(user_sid)));
		cont(private_data, False, NULL, 0);
		return;
	}

	r = TALLOC_P(state, struct winbind_lookup);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_lookup_level);
	if (!r->in.level) goto nomem;

	*r->in.level	= WINBIND_LOOKUP_LEVEL_SID2DOMGROUPS;
	r->in.req.sid	= &state->user_sid;
	if (!r->in.req.sid) goto nomem;

	do_async_ndr_domain(mem_ctx, domain,
			    NDR_WINBIND_LOOKUP, r,
			    gettoken_recvdomgroups, state,
			    NULL, NULL);
	return;
nomem:
	DEBUG(5, ("%s: no memory\n",__location__));
	cont(private_data, False, NULL, 0);
}

static void gettoken_recvdomgroups(TALLOC_CTX *mem_ctx, bool success,
				   struct winbindd_ndr_call *c,
				   void *_private_data,
				   void *_cont,
				   void *cont_private)
{
	struct gettoken_state *state =
		talloc_get_type_abort(_private_data, struct gettoken_state);
	struct winbind_lookup *r =
		talloc_get_type_abort(c->ndr.r, struct winbind_lookup);
	uint32_t i;

	if (!success) {
		DEBUG(10, ("Could not get domain groups\n"));
		TALLOC_FREE(r);
		state->cont(state->private_data, false, NULL, 0);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("Could not trigger lookup(sid2domgroups): 0x%08X\n",
			r->out.result));
		TALLOC_FREE(r);
		state->cont(state->private_data, false, NULL, 0);
		return;
	}

	if (state->num_sids == 0) {
		/* This could be normal if we are dealing with a
		   local user and local groups */

		if ( !sid_check_is_in_our_domain( &state->user_sid ) ) {
			DEBUG(10, ("Received no domain groups\n"));
			state->cont(state->private_data, True, NULL, 0);
			return;
		}
	}

	state->sids = NULL;
	state->num_sids = 0;

	if (!NT_STATUS_IS_OK(add_sid_to_array(mem_ctx, &state->user_sid,
					      &state->sids, &state->num_sids)))
	{
		DEBUG(0, ("Out of memory\n"));
		state->cont(state->private_data, False, NULL, 0);
		return;
	}

	for (i=0; i < r->out.rep->sid_array.num_sids; i++) {
		if (!NT_STATUS_IS_OK(add_sid_to_array(mem_ctx, r->out.rep->sid_array.sids[i].sid,
						      &state->sids, &state->num_sids)))
		{
			DEBUG(0, ("Out of memory\n"));
			state->cont(state->private_data, false, NULL, 0);
			return;
		}
	}

	if (state->alias_domain == NULL) {
		DEBUG(10, ("Don't expand domain local groups\n"));
		state->cont(state->private_data, True, state->sids,
			    state->num_sids);
		return;
	}

	winbindd_getsidaliases_async(state->alias_domain, mem_ctx,
				     state->sids, state->num_sids,
				     gettoken_recvaliases, state);
}

static void gettoken_recvaliases(void *private_data, bool success,
				 struct winbind_lookup_sid_info_array *aliases)
{
	struct gettoken_state *state = (struct gettoken_state *)private_data;
	size_t i;

	if (!success) {
		DEBUG(10, ("Could not receive domain local groups\n"));
		state->cont(state->private_data, False, NULL, 0);
		return;
	}

	for (i=0; aliases && i < aliases->num_sids; i++) {
		if (!NT_STATUS_IS_OK(add_sid_to_array(state->mem_ctx,
						      aliases->sids[i].sid,
						      &state->sids,
						      &state->num_sids)))
		{
			DEBUG(0, ("Out of memory\n"));
			state->cont(state->private_data, False, NULL, 0);
			return;
		}
	}

	if (state->local_alias_domain != NULL) {
		struct winbindd_domain *local_domain = state->local_alias_domain;
		DEBUG(10, ("Expanding our own local groups\n"));
		state->local_alias_domain = NULL;
		winbindd_getsidaliases_async(local_domain, state->mem_ctx,
					     state->sids, state->num_sids,
					     gettoken_recvaliases, state);
		return;
	}

	if (state->builtin_domain != NULL) {
		struct winbindd_domain *builtin_domain = state->builtin_domain;
		DEBUG(10, ("Expanding our own BUILTIN groups\n"));
		state->builtin_domain = NULL;
		winbindd_getsidaliases_async(builtin_domain, state->mem_ctx,
					     state->sids, state->num_sids,
					     gettoken_recvaliases, state);
		return;
	}

	state->cont(state->private_data, True, state->sids, state->num_sids);
}

static void query_user_recv(TALLOC_CTX *mem_ctx, bool success,
			    struct winbindd_ndr_call *c,
			    void *_private_data,
			    void *_cont,
			    void *cont_private)
{
	void (*cont)(void *private_data, bool success,
		     const char *acct_name,
		     const char *full_name,
		     const char *homedir,
		     const char *shell,
		     gid_t gid,
		     uint32 group_rid) =
		(void (*)(void *, bool, const char *, const char *,
			const char *,const char *, gid_t, uint32_t))_cont;
	struct winbind_lookup *r =
		talloc_get_type_abort(_private_data, struct winbind_lookup);

	if (!success) {
		DEBUG(5, ("Could not trigger lookup(sid2userinfo)\n"));
		TALLOC_FREE(r);
		cont(cont_private, false, NULL, NULL, NULL, NULL, (gid_t)-1, 0);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("Could not trigger lookup(sid2userinfo): 0x%08X\n",
			r->out.result));
		TALLOC_FREE(r);
		cont(cont_private, false, NULL, NULL, NULL, NULL, (gid_t)-1, 0);
		return;
	}

	cont(cont_private, true,
	     r->out.rep->user_info.account,
	     r->out.rep->user_info.gecos,
	     r->out.rep->user_info.homedir,
	     r->out.rep->user_info.shell,
	     r->out.rep->user_info.primary_gid,
	     r->out.rep->user_info.primary_rid);
}

void query_user_async(TALLOC_CTX *mem_ctx, struct winbindd_domain *domain,
		      const DOM_SID *sid,
		      void (*cont)(void *private_data, bool success,
				   const char *acct_name,
				   const char *full_name,
				   const char *homedir,
				   const char *shell,
				   gid_t gid,
				   uint32 group_rid),
		      void *private_data)
{
	struct winbind_lookup *r = NULL;

	r = TALLOC_P(mem_ctx, struct winbind_lookup);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_lookup_level);
	if (!r->in.level) goto nomem;

	*r->in.level	= WINBIND_LOOKUP_LEVEL_SID2USERINFO;
	r->in.req.sid	= sid_dup_talloc(r, sid);
	if (!r->in.req.sid) goto nomem;

	do_async_ndr_domain(mem_ctx, domain,
			    NDR_WINBIND_LOOKUP, r,
			    query_user_recv, r,
			    (void *)cont, private_data);
	return;
nomem:
	TALLOC_FREE(r);
	cont(private_data, false, NULL, NULL, NULL, NULL, (gid_t)-1, 0);
	return;
}
