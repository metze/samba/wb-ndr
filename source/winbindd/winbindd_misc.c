/* 
   Unix SMB/CIFS implementation.

   Winbind daemon - miscellaneous other functions

   Copyright (C) Tim Potter      2000
   Copyright (C) Andrew Bartlett 2002
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "winbindd.h"

#undef DBGC_CLASS
#define DBGC_CLASS DBGC_WINBIND

/* Check the machine account password is valid */

static void winbindd_check_machine_acct_recv(TALLOC_CTX *mem_ctx, bool success,
					     struct winbindd_ndr_call *c,
					     void *private_data,
					     void *_cont,
					     void *_cont_private)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);
	struct winbind_trust *r =
		talloc_get_type_abort(c->ndr.r, struct winbind_trust);

	if (!success) {
		DEBUG(5, ("Could not check machine acct\n"));
		request_error(state);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("check machine acct returned an error:0x%08X\n",
			r->out.result));
		request_error(state);
		return;
	}

	request_ok(state);
}

void winbindd_check_machine_acct(struct winbindd_cli_state *state)
{
	struct winbind_trust *r;

	DEBUG(3, ("[%5lu]: check machine account\n",
		  (unsigned long)state->pid));

	r = TALLOC_P(state->mem_ctx, struct winbind_trust);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_trust_level);
	if (!r->in.level) goto nomem;

	*r->in.level	= WINBIND_TRUST_LEVEL_COMPAT_CHECK_MACHCC;

	do_async_ndr_domain(state->mem_ctx, find_our_domain(),
			    NDR_WINBIND_TRUST, r,
			    winbindd_check_machine_acct_recv, state,
			    NULL, NULL);
	return;
nomem:
	DEBUG(0, ("talloc failed\n"));
	request_error(state);
}


/* Constants and helper functions for determining domain trust types */

enum trust_type {
	EXTERNAL = 0,
	FOREST,
	IN_FOREST,
	NONE,
};

const char *trust_type_strings[] = {"External", 
				    "Forest", 
				    "In Forest",
				    "None"};

static enum trust_type get_trust_type(struct winbindd_tdc_domain *domain)
{
	if (domain->trust_attribs == NETR_TRUST_ATTRIBUTE_QUARANTINED_DOMAIN)	
		return EXTERNAL;
	else if (domain->trust_attribs == NETR_TRUST_ATTRIBUTE_FOREST_TRANSITIVE)
		return FOREST;
	else if (((domain->trust_flags & NETR_TRUST_FLAG_IN_FOREST) == NETR_TRUST_FLAG_IN_FOREST) &&
	    ((domain->trust_flags & NETR_TRUST_FLAG_PRIMARY) == 0x0))
		return IN_FOREST;
	return NONE;	
}

static const char *get_trust_type_string(struct winbindd_tdc_domain *domain)
{
	return trust_type_strings[get_trust_type(domain)];
}

static bool trust_is_inbound(struct winbindd_tdc_domain *domain)
{
	return (domain->trust_flags == 0x0) ||
	    ((domain->trust_flags & NETR_TRUST_FLAG_IN_FOREST) ==
            NETR_TRUST_FLAG_IN_FOREST) ||           		
	    ((domain->trust_flags & NETR_TRUST_FLAG_INBOUND) ==
	    NETR_TRUST_FLAG_INBOUND);      	
}

static bool trust_is_outbound(struct winbindd_tdc_domain *domain)
{
	return (domain->trust_flags == 0x0) ||
	    ((domain->trust_flags & NETR_TRUST_FLAG_IN_FOREST) ==
            NETR_TRUST_FLAG_IN_FOREST) ||           		
	    ((domain->trust_flags & NETR_TRUST_FLAG_OUTBOUND) ==
	    NETR_TRUST_FLAG_OUTBOUND);      	
}

static bool trust_is_transitive(struct winbindd_tdc_domain *domain)
{
	if ((domain->trust_attribs == NETR_TRUST_ATTRIBUTE_NON_TRANSITIVE) ||         
	    (domain->trust_attribs == NETR_TRUST_ATTRIBUTE_QUARANTINED_DOMAIN) ||
	    (domain->trust_attribs == NETR_TRUST_ATTRIBUTE_TREAT_AS_EXTERNAL))
		return False;
	return True;
}

void winbindd_list_trusted_domains(struct winbindd_cli_state *state)
{
	struct winbindd_tdc_domain *dom_list = NULL;
	struct winbindd_tdc_domain *d = NULL;
	size_t num_domains = 0;
	int extra_data_len = 0;
	char *extra_data = NULL;
	int i = 0;
	
	DEBUG(3, ("[%5lu]: list trusted domains\n",
		  (unsigned long)state->pid));

	if( !wcache_tdc_fetch_list( &dom_list, &num_domains )) {
		request_error(state);	
		goto done;
	}

	for ( i = 0; i < num_domains; i++ ) {
		struct winbindd_domain *domain;
		bool is_online = true;		

		d = &dom_list[i];
		domain = find_domain_from_name_noinit(d->domain_name);
		if (domain) {
			is_online = domain->online;
		}

		if ( !extra_data ) {
			extra_data = talloc_asprintf(state->mem_ctx, 
						     "%s\\%s\\%s\\%s\\%s\\%s\\%s\\%s",
						     d->domain_name,
						     d->dns_name ? d->dns_name : d->domain_name,
						     sid_string_talloc(state->mem_ctx, &d->sid),
						     get_trust_type_string(d),
						     trust_is_transitive(d) ? "Yes" : "No",
						     trust_is_inbound(d) ? "Yes" : "No",
						     trust_is_outbound(d) ? "Yes" : "No",
						     is_online ? "Online" : "Offline" );
		} else {
			extra_data = talloc_asprintf(state->mem_ctx, 
						     "%s\n%s\\%s\\%s\\%s\\%s\\%s\\%s\\%s",
						     extra_data,
						     d->domain_name,
						     d->dns_name ? d->dns_name : d->domain_name,
						     sid_string_talloc(state->mem_ctx, &d->sid),
						     get_trust_type_string(d),
						     trust_is_transitive(d) ? "Yes" : "No",
						     trust_is_inbound(d) ? "Yes" : "No",
						     trust_is_outbound(d) ? "Yes" : "No",
						     is_online ? "Online" : "Offline" );
		}
	}
	
	extra_data_len = 0;
	if (extra_data != NULL) {
		extra_data_len = strlen(extra_data);
	}

	if (extra_data_len > 0) {
		state->response.extra_data.data = SMB_STRDUP(extra_data);
		state->response.length += extra_data_len+1;
	}

	request_ok(state);	
done:
	TALLOC_FREE( dom_list );
	TALLOC_FREE( extra_data );	
}

static void ndr_child_trust_compat_list(struct winbindd_domain *domain,
					struct winbindd_cli_state *state,
					struct winbind_trust *r)
{
	uint32_t i, num_domains;
	char **names, **alt_names;
	DOM_SID *sids;
	NTSTATUS result;
	bool have_own_domain = False;
	struct winbind_domain_info_compat *t;

	DEBUG(3, ("list trusted domains\n"));

	result = domain->methods->trusted_domains(domain, state->mem_ctx,
						  &num_domains, &names,
						  &alt_names, &sids);

	if (!NT_STATUS_IS_OK(result)) {
		DEBUG(3, ("winbindd_dual_list_trusted_domains: trusted_domains returned %s\n",
			nt_errstr(result) ));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	t = talloc_array(r->out.rep,
			 struct winbind_domain_info_compat,
			 num_domains + 1);
	if (!t) {
		r->out.result = WINBIND_STATUS_NO_MEMORY;
		return;
	}

	for (i=0; i<num_domains; i++) {
		t[i].netbios_name	= names[i];
		t[i].dns_name		= alt_names[i] ?
					  alt_names[i] :
					  names[i];
		t[i].sid		= &sids[i];
		/* TODO: fill this in */
		t[i].is_native_mode	= false;
		t[i].is_active_directory= false;
		t[i].is_primary		= false;

		if (strequal(names[i], domain->name)) {
			have_own_domain = True;
			t[i].is_native_mode	= domain->native_mode;
			t[i].is_active_directory= domain->active_directory;
			t[i].is_primary		= domain->primary;
		}
	}

	/* add our primary domain */
	if (!have_own_domain) {
		t[i].netbios_name	= domain->name;
		t[i].dns_name		= domain->alt_name ?
					  domain->alt_name :
					  domain->name;
		t[i].sid		= &domain->sid;
		t[i].is_native_mode	= domain->native_mode;
		t[i].is_active_directory= domain->active_directory;
		t[i].is_primary		= domain->primary;
		i++;
	}

	r->out.rep->compat_trusts.num_domains	= i;
	r->out.rep->compat_trusts.domains	= t;
	r->out.result = WINBIND_STATUS_OK;
}

static void ndr_child_trust_compat_check_machcc(struct winbindd_domain *domain,
						struct winbindd_cli_state *state,
						struct winbind_trust *r)
{
	NTSTATUS result = NT_STATUS_UNSUCCESSFUL;
	int num_retries = 0;
	struct winbindd_domain *contact_domain;

	DEBUG(3, ("check machine account\n"));

	/* Get trust account password */

 again:

	contact_domain = find_our_domain();

        /* This call does a cli_nt_setup_creds() which implicitly checks
           the trust account password. */

	invalidate_cm_connection(&contact_domain->conn);

	{
		struct rpc_pipe_client *netlogon_pipe;
		result = cm_connect_netlogon(contact_domain, &netlogon_pipe);
	}

	if (!NT_STATUS_IS_OK(result)) {
		DEBUG(3, ("could not open handle to NETLOGON pipe\n"));
		goto done;
	}

	/* There is a race condition between fetching the trust account
	   password and the periodic machine password change.  So it's
	   possible that the trust account password has been changed on us.
	   We are returned NT_STATUS_ACCESS_DENIED if this happens. */

#define MAX_RETRIES 8

        if ((num_retries < MAX_RETRIES) &&
	    NT_STATUS_V(result) == NT_STATUS_V(NT_STATUS_ACCESS_DENIED)) {
		num_retries++;
		goto again;
        }

	/* Pass back result code - zero for success, other values for
	   specific failures. */

	DEBUG(3,("secret is %s\n", NT_STATUS_IS_OK(result) ?
		"good" : "bad"));

 done:

	DEBUG(NT_STATUS_IS_OK(result) ? 5 : 2,
	      ("Checking the trust account password returned %s\n",
	       nt_errstr(result)));

	if (NT_STATUS_IS_OK(result)) {
		r->out.result = WINBIND_STATUS_OK;
	} else {
		r->out.result = WINBIND_STATUS_FOOBAR;
	}
}

void winbindd_ndr_domain_child_trust(struct winbindd_domain *domain,
				     struct winbindd_cli_state *state)
{
	struct winbind_trust *r;

	r = talloc_get_type_abort(state->c.ndr.r,
				  struct winbind_trust);

	switch (*r->in.level) {
	case WINBIND_TRUST_LEVEL_COMPAT_LIST:
		ndr_child_trust_compat_list(domain, state, r);
		return;
	case WINBIND_TRUST_LEVEL_COMPAT_CHECK_MACHCC:
		ndr_child_trust_compat_check_machcc(domain, state, r);
		return;
	}

	r->out.result = WINBIND_STATUS_UNKNOWN_LEVEL;
	return;
}

static void winbindd_getdcname_recv(void *private_data,
				    bool success,
				    struct winbind_get_dc_info *r)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);

	if (!success) {
		request_error(state);
		return;
	}

	fstrcpy(state->response.data.dc_name, r->out.dc_info->name);

	request_ok(state);
}

void winbindd_getdcname(struct winbindd_cli_state *state)
{
	struct winbindd_domain *domain;
	struct winbind_get_dc_info *r;

	state->request.domain_name
		[sizeof(state->request.domain_name)-1] = '\0';

	DEBUG(3, ("[%5lu]: Get DC name for %s\n", (unsigned long)state->pid,
		  state->request.domain_name));

	domain = find_domain_from_name_noinit(state->request.domain_name);
	if (domain && (domain->internal || sid_check_is_domain(&domain->sid))) {
		fstrcpy(state->response.data.dc_name, global_myname());
		request_ok(state);	
		return;
	}

	r = TALLOC_P(state->mem_ctx, struct winbind_get_dc_info);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_dc_info_level);
	if (!r->in.level) goto nomem;

	*r->in.level		= WINBIND_DC_INFO_LEVEL_COMPAT_NT4;
	r->in.domain_name	= state->request.domain_name;

	winbindd_get_dc_info_async_domain(state->mem_ctx, find_our_domain(),
					  r, winbindd_getdcname_recv, state);
	return;
nomem:
	request_error(state);
	return;
}

static void winbindd_get_dc_info_recv(TALLOC_CTX *mem_ctx, bool success,
				      struct winbindd_ndr_call *c,
				      void *_r,
				      void *_cont,
				      void *private_data)
{
	void (*cont)(void *priv, bool succ, struct winbind_get_dc_info *r) =
		(void (*)(void *, bool, struct winbind_get_dc_info*))_cont;
	struct winbind_get_dc_info *r =
		talloc_get_type_abort(_r, struct winbind_get_dc_info);

	if (!success) {
		DEBUG(5, ("Could not get dc_info\n"));
		cont(private_data, False, r);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("get_dc_info returned an error:0x%08X\n",
			r->out.result));
		cont(private_data, False, r);
		return;
	}

	cont(private_data, True, r);
}

void winbindd_get_dc_info_async_domain(TALLOC_CTX *mem_ctx,
				       struct winbindd_domain *domain,
				       struct winbind_get_dc_info *r,
				       void (*cont)(void *private_data,
						    bool success,
						    struct winbind_get_dc_info *r),
				       void *private_data)
{
	do_async_ndr_domain(mem_ctx, domain,
			    NDR_WINBIND_GET_DC_INFO, r,
			    winbindd_get_dc_info_recv, r,
			    (void *)cont, private_data);
}

void winbindd_get_dc_info_async_child(TALLOC_CTX *mem_ctx,
				      struct winbindd_child *child,
				      struct winbind_get_dc_info *r,
				      void (*cont)(void *private_data,
						   bool success,
						   struct winbind_get_dc_info *r),
				      void *private_data)
{
	do_async_ndr(mem_ctx, child,
		     NDR_WINBIND_GET_DC_INFO, r,
		     winbindd_get_dc_info_recv, r,
		     (void *)cont, private_data);
}

static void ndr_child_get_dc_info_comapt_nt4(struct winbindd_domain *domain,
					     struct winbindd_cli_state *state,
					     struct winbind_get_dc_info *r)
{
	const char *dcname_slash;
	const char *p;
	struct rpc_pipe_client *netlogon_pipe;
	NTSTATUS result;
	WERROR werr;
	unsigned int orig_timeout;
	struct winbindd_domain *req_domain;

	DEBUG(3, ("Get DC name for '%s'\n", r->in.domain_name));

	result = cm_connect_netlogon(domain, &netlogon_pipe);

	if (!NT_STATUS_IS_OK(result)) {
		DEBUG(1, ("Can't contact the NETLOGON pipe\n"));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	/* This call can take a long time - allow the server to time out.
	   35 seconds should do it. */

	orig_timeout = cli_set_timeout(netlogon_pipe->cli, 35000);

	/*
	 * if the requested domain name matches the current one
	 * we need to use GetDCName(), because GetAnyDCName() only
	 * works for trusted domains
	 */
	req_domain = find_domain_from_name_noinit(r->in.domain_name);
	if (req_domain == domain) {
		result = rpccli_netr_GetDcName(netlogon_pipe,
					       r,
					       domain->dcname,
					       r->in.domain_name,
					       &dcname_slash,
					       &werr);
	} else {
		result = rpccli_netr_GetAnyDCName(netlogon_pipe,
						  r,
						  domain->dcname,
						  r->in.domain_name,
						  &dcname_slash,
						  &werr);
	}
	/* And restore our original timeout. */
	cli_set_timeout(netlogon_pipe->cli, orig_timeout);

	if (!NT_STATUS_IS_OK(result)) {
		DEBUG(5,("Error requesting DCname for domain %s: %s\n",
			r->in.domain_name, nt_errstr(result)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	if (!W_ERROR_IS_OK(werr)) {
		DEBUG(5, ("Error requesting DCname for domain %s: %s\n",
			r->in.domain_name, dos_errstr(werr)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	p = dcname_slash;
	if (*p == '\\') {
		p+=1;
	}
	if (*p == '\\') {
		p+=1;
	}

	r->out.dc_info->name = talloc_strdup(r, p);
	if (!r->out.dc_info->name) {
		r->out.result = WINBIND_STATUS_NO_MEMORY;
		return;
	}
	r->out.result = WINBIND_STATUS_OK;
}

void winbindd_ndr_domain_child_get_dc_info(struct winbindd_domain *domain,
					   struct winbindd_cli_state *state)
{
	struct winbind_get_dc_info *r;

	r = talloc_get_type_abort(state->c.ndr.r,
				  struct winbind_get_dc_info);

	switch (*r->in.level) {
	case WINBIND_DC_INFO_LEVEL_COMPAT_NT4:
		ndr_child_get_dc_info_comapt_nt4(domain, state, r);
		return;

	case WINBIND_DC_INFO_LEVEL_COMPAT_DS:
		r->out.result = WINBIND_STATUS_INVALID_LEVEL;
		return;
	}

	r->out.result = WINBIND_STATUS_UNKNOWN_LEVEL;
	return;
}

static void winbindd_get_domain_info_recv(TALLOC_CTX *mem_ctx, bool success,
					  struct winbindd_ndr_call *c,
					  void *_r,
					  void *_cont,
					  void *private_data)
{
	void (*cont)(void *priv, bool succ, struct winbind_get_domain_info *r) =
		(void (*)(void *, bool, struct winbind_get_domain_info*))_cont;
	struct winbind_get_domain_info *r =
		talloc_get_type_abort(_r, struct winbind_get_domain_info);

	if (!success) {
		DEBUG(5, ("Could not get domain_info\n"));
		cont(private_data, False, r);
		return;
	}

	if (r->out.result != WINBIND_STATUS_OK) {
		DEBUG(5, ("get_domain_info returned an error:0x%08X\n",
			r->out.result));
		cont(private_data, False, r);
		return;
	}

	cont(private_data, True, r);
}

void winbindd_get_domain_info_async(TALLOC_CTX *mem_ctx,
				    struct winbindd_domain *domain,
				    struct winbind_get_domain_info *r,
				    void (*cont)(void *private_data,
						 bool success,
						 struct winbind_get_domain_info *r),
				    void *private_data)
{
	do_async_ndr_domain(mem_ctx, domain,
			    NDR_WINBIND_GET_DOMAIN_INFO, r,
			    winbindd_get_domain_info_recv, r,
			    (void *)cont, private_data);
}

struct sequence_state {
	struct winbindd_cli_state *cli_state;
	struct winbindd_domain *domain;
	struct winbind_get_domain_info *r;
	char *extra_data;
	void (*recv_fn)(void *priv, bool succ,
			struct winbind_get_domain_info *r);
};

static void sequence_one_recv(void *private_data, bool success,
			      struct winbind_get_domain_info *r);
static void sequence_next_recv(void *private_data, bool success,
			       struct winbind_get_domain_info *r);

void winbindd_show_sequence(struct winbindd_cli_state *state)
{
	struct sequence_state *seq;

	/* Ensure null termination */
	state->request.domain_name[sizeof(state->request.domain_name)-1]='\0';

	seq = TALLOC_P(state->mem_ctx, struct sequence_state);
	if (!seq) goto nomem;
	seq->cli_state = state;
	seq->extra_data = talloc_strdup(state->mem_ctx, "");
	if (!seq->extra_data) goto nomem;
	seq->r = TALLOC_P(seq, struct winbind_get_domain_info);
	if (!seq->r) goto nomem;
	seq->r->in.level = TALLOC_P(seq->r, enum winbind_domain_info_level);
	if (!seq->r->in.level) goto nomem;

	if (strlen(state->request.domain_name) > 0) {
		seq->domain = find_domain_from_name_noinit(
			state->request.domain_name);
		if (seq->domain == NULL) {
			DEBUG(0,("domain '%s' not found\n",
				 state->request.domain_name));
			request_error(state);
			return;
		}
		seq->recv_fn = sequence_one_recv;
	} else {
		seq->domain = domain_list();
		if (seq->domain == NULL) {
			DEBUG(0, ("domain list empty\n"));
			request_error(state);
			return;
		}
		seq->recv_fn = sequence_next_recv;
	}

	*seq->r->in.level	= WINBIND_DOMAIN_INFO_LEVEL_SEQNUM;
	seq->r->in.domain_name	= seq->domain->name;
	seq->r->in.dc_name	= NULL;

	winbindd_get_domain_info_async(seq, seq->domain, seq->r,
				       seq->recv_fn, seq);
	return;
nomem:
	request_error(state);
	return;
}

static void sequence_one_recv(void *private_data, bool success,
			      struct winbind_get_domain_info *r)
{
	struct sequence_state *state = talloc_get_type_abort(private_data,
				       struct sequence_state);

	if (!success) {
		request_error(state->cli_state);
		return;
	}

	state->cli_state->response.data.sequence_number =
		state->r->out.domain_info->seqnum;

	request_ok(state->cli_state);
}

static void sequence_next_recv(void *private_data, bool success,
			       struct winbind_get_domain_info *r)
{
	struct sequence_state *state = talloc_get_type_abort(private_data,
				       struct sequence_state);
	uint64_t seq = DOM_SEQUENCE_NONE;

	if (success) {
		seq = state->r->out.domain_info->seqnum;
	}

	if (seq == DOM_SEQUENCE_NONE) {
		state->extra_data = talloc_asprintf(state,
						    "%s%s : DISCONNECTED\n",
						    state->extra_data,
						    state->domain->name);
	} else {
		state->extra_data = talloc_asprintf(state,
						    "%s%s : %lld\n",
						    state->extra_data,
						    state->domain->name,
						    (unsigned long long)seq);
	}

	state->domain->sequence_number = seq;

	state->domain = state->domain->next;

	if (state->domain == NULL) {
		struct winbindd_cli_state *cli_state = state->cli_state;
		cli_state->response.length =
			sizeof(cli_state->response) +
			strlen(state->extra_data) + 1;
		cli_state->response.extra_data.data =
			SMB_STRDUP(state->extra_data);
		request_ok(cli_state);
		return;
	}

	/* Ask the next domain */
	*state->r->in.level	= WINBIND_DOMAIN_INFO_LEVEL_SEQNUM;
	state->r->in.domain_name= state->domain->name;
	state->r->in.dc_name	= NULL;

	winbindd_get_domain_info_async(state, state->domain, state->r,
				       state->recv_fn, state);
}

struct domain_info_state {
	struct winbindd_domain *domain;
	struct winbindd_cli_state *cli_state;
};

static void domain_info_init_recv(void *private_data, bool success);

void winbindd_domain_info(struct winbindd_cli_state *state)
{
	struct winbindd_domain *domain;

	DEBUG(3, ("[%5lu]: domain_info [%s]\n", (unsigned long)state->pid,
		  state->request.domain_name));

	domain = find_domain_from_name_noinit(state->request.domain_name);

	if (domain == NULL) {
		DEBUG(3, ("Did not find domain [%s]\n",
			  state->request.domain_name));
		request_error(state);
		return;
	}

	if (!domain->initialized) {
		struct domain_info_state *istate;

		istate = TALLOC_P(state->mem_ctx, struct domain_info_state);
		if (istate == NULL) {
			DEBUG(0, ("talloc failed\n"));
			request_error(state);
			return;
		}

		istate->cli_state = state;
		istate->domain = domain;

		init_child_connection(domain, domain_info_init_recv, istate);
				      
		return;
	}

	fstrcpy(state->response.data.domain_info.name,
		domain->name);
	fstrcpy(state->response.data.domain_info.alt_name,
		domain->alt_name);
	sid_to_fstring(state->response.data.domain_info.sid, &domain->sid);
	
	state->response.data.domain_info.native_mode =
		domain->native_mode;
	state->response.data.domain_info.active_directory =
		domain->active_directory;
	state->response.data.domain_info.primary =
		domain->primary;

	request_ok(state);
}

static void domain_info_init_recv(void *private_data, bool success)
{
	struct domain_info_state *istate =
		(struct domain_info_state *)private_data;
	struct winbindd_cli_state *state = istate->cli_state;
	struct winbindd_domain *domain = istate->domain;

	DEBUG(10, ("Got back from child init: %d\n", success));

	if ((!success) || (!domain->initialized)) {
		DEBUG(5, ("Could not init child for domain %s\n",
			  domain->name));
		request_error(state);
		return;
	}

	fstrcpy(state->response.data.domain_info.name,
		domain->name);
	fstrcpy(state->response.data.domain_info.alt_name,
		domain->alt_name);
	sid_to_fstring(state->response.data.domain_info.sid, &domain->sid);
	
	state->response.data.domain_info.native_mode =
		domain->native_mode;
	state->response.data.domain_info.active_directory =
		domain->active_directory;
	state->response.data.domain_info.primary =
		domain->primary;

	request_ok(state);
}

void winbindd_ping(struct winbindd_cli_state *state)
{
	if (lp_parm_bool(-1, "winbindd", "ping_our_domain", False)) {
		sendto_domain(state, find_our_domain());
		return;
	}

	DEBUG(3, ("[%5lu]: ping\n", (unsigned long)state->pid));
	request_ok(state);
}

enum winbindd_result winbindd_dual_ping(struct winbindd_domain *domain,
					struct winbindd_cli_state *state)
{
	DEBUG(3, ("[%5lu]: (dual) ping\n", (unsigned long)state->pid));
	return WINBINDD_OK;
}

/* List various tidbits of information */

void winbindd_info(struct winbindd_cli_state *state)
{

	DEBUG(3, ("[%5lu]: request misc info\n", (unsigned long)state->pid));

	state->response.data.info.winbind_separator = *lp_winbind_separator();
	fstrcpy(state->response.data.info.samba_version, SAMBA_VERSION_STRING);
	request_ok(state);
}

/* Tell the client the current interface version */

void winbindd_interface_version(struct winbindd_cli_state *state)
{
	DEBUG(3, ("[%5lu]: request interface version\n",
		  (unsigned long)state->pid));
	
	state->response.data.interface_version = WINBIND_INTERFACE_VERSION;
	request_ok(state);
}

/* What domain are we a member of? */

void winbindd_domain_name(struct winbindd_cli_state *state)
{
	DEBUG(3, ("[%5lu]: request domain name\n", (unsigned long)state->pid));
	
	fstrcpy(state->response.data.domain_name, lp_workgroup());
	request_ok(state);
}

/* What's my name again? */

void winbindd_netbios_name(struct winbindd_cli_state *state)
{
	DEBUG(3, ("[%5lu]: request netbios name\n",
		  (unsigned long)state->pid));
	
	fstrcpy(state->response.data.netbios_name, global_myname());
	request_ok(state);
}

/* Where can I find the privilaged pipe? */

void winbindd_priv_pipe_dir(struct winbindd_cli_state *state)
{

	DEBUG(3, ("[%5lu]: request location of privileged pipe\n",
		  (unsigned long)state->pid));
	
	state->response.extra_data.data = SMB_STRDUP(get_winbind_priv_pipe_dir());
	if (!state->response.extra_data.data) {
		DEBUG(0, ("malloc failed\n"));
		request_error(state);
		return;
	}

	/* must add one to length to copy the 0 for string termination */
	state->response.length +=
		strlen((char *)state->response.extra_data.data) + 1;

	request_ok(state);
}

