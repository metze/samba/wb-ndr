/*
   Unix SMB/CIFS implementation.

   Winbind daemon - miscellaneous other functions

   Copyright (C) Tim Potter      2000
   Copyright (C) Andrew Bartlett 2002

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "winbindd.h"

#undef DBGC_CLASS
#define DBGC_CLASS DBGC_WINBIND


static const struct winbindd_child_dispatch_table locator_dispatch_table[];

static struct winbindd_child static_locator_child;

void init_locator_child(void)
{
	setup_child(&static_locator_child,
		    locator_dispatch_table,
		    "log.winbindd", "locator");
}

struct winbindd_child *locator_child(void)
{
	return &static_locator_child;
}

static void winbindd_dsgetdcname_recv(void *private_data,
				      bool success,
				      struct winbind_get_dc_info *r)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);

	if (!success) {
		request_error(state);
		return;
	}

	fstrcpy(state->response.data.dc_name, r->out.dc_info->name);

	request_ok(state);
}

void winbindd_dsgetdcname(struct winbindd_cli_state *state)
{
	struct winbind_get_dc_info *r;

	state->request.domain_name
		[sizeof(state->request.domain_name)-1] = '\0';

	DEBUG(3, ("[%5lu]: dsgetdcname for %s\n", (unsigned long)state->pid,
		  state->request.domain_name));

	r = TALLOC_P(state->mem_ctx, struct winbind_get_dc_info);
	if (!r) goto nomem;
	r->in.level = TALLOC_P(r, enum winbind_dc_info_level);
	if (!r->in.level) goto nomem;

	*r->in.level		= WINBIND_DC_INFO_LEVEL_COMPAT_DS;
	r->in.domain_name	= state->request.domain_name;
	r->in.params.flags	= state->request.flags;

	winbindd_get_dc_info_async_child(state->mem_ctx, locator_child(),
					 r, winbindd_dsgetdcname_recv, state);
	return;
nomem:
	request_error(state);
	return;
}

static void ndr_child_get_dc_info_comapt_ds(struct winbindd_domain *domain,
					    struct winbindd_cli_state *state,
					    struct winbind_get_dc_info *r)
{
	NTSTATUS result;
	struct netr_DsRGetDCNameInfo *info = NULL;
	const char *dc = NULL;

	DEBUG(3, ("dsgetdcname for '%s'\n", r->in.domain_name));

	result = dsgetdcname(state->mem_ctx, r->in.domain_name,
			     NULL, NULL, r->in.params.flags, &info);
	if (!NT_STATUS_IS_OK(result)) {
		DEBUG(1, ("dsgetdcname failed: %s\n", nt_errstr(result)));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	if (info->dc_address) {
		dc = info->dc_address;
		if ((dc[0] == '\\') && (dc[1] == '\\')) {
			dc += 2;
		}
	}

	if ((!dc || !is_ipaddress_v4(dc)) && info->dc_unc) {
		dc = info->dc_unc;
	}

	if (!dc || !*dc) {
		TALLOC_FREE(info);
		DEBUG(1, ("DsGetDcName failed: no dc name\n"));
		r->out.result = WINBIND_STATUS_FOOBAR;
		return;
	}

	r->out.dc_info->name = talloc_strdup(r, dc);
	TALLOC_FREE(info);
	if (!r->out.dc_info->name) {
		r->out.result = WINBIND_STATUS_NO_MEMORY;
		return;
	}
	r->out.result = WINBIND_STATUS_OK;
}

void winbindd_ndr_locator_child_get_dc_info(struct winbindd_domain *domain,
					    struct winbindd_cli_state *state)
{
	struct winbind_get_dc_info *r;

	r = talloc_get_type_abort(state->c.ndr.r,
				  struct winbind_get_dc_info);

	switch (*r->in.level) {
	case WINBIND_DC_INFO_LEVEL_COMPAT_NT4:
		r->out.result = WINBIND_STATUS_INVALID_LEVEL;
		return;

	case WINBIND_DC_INFO_LEVEL_COMPAT_DS:
		ndr_child_get_dc_info_comapt_ds(domain, state, r);
		return;
	}

	r->out.result = WINBIND_STATUS_UNKNOWN_LEVEL;
	return;
}

static const struct winbindd_child_dispatch_table locator_dispatch_table[] = {
	{
		.name		= "NDR_WINBIND_PING",
		.ndr_opnum	= NDR_WINBIND_PING,
		.ndr_fn		= winbindd_ndr_child_ping,
	},{
		.name		= "NDR_WINBIND_GET_DC_INFO",
		.ndr_opnum	= NDR_WINBIND_GET_DC_INFO,
		.ndr_fn		= winbindd_ndr_locator_child_get_dc_info,
	},{
		.name		= NULL,
	}
};
