/* 
   Unix SMB/CIFS implementation.

   fast routines for getting the wire size of security objects

   Copyright (C) Andrew Tridgell 2003
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "includes.h"

/*
  return the wire size of a dom_sid
*/
size_t ndr_size_dom_sid(const struct dom_sid *sid, int flags)
{
	if (!sid) return 0;
	return 8 + 4*sid->num_auths;
}

size_t ndr_size_dom_sid28(const struct dom_sid *sid, int flags)
{
	struct dom_sid zero_sid;

	if (!sid) return 0;

	ZERO_STRUCT(zero_sid);

	if (memcmp(&zero_sid, sid, sizeof(zero_sid)) == 0) {
		return 0;
	}

	return 8 + 4*sid->num_auths;
}

size_t ndr_size_dom_sid0(const struct dom_sid *sid, int flags)
{
	return ndr_size_dom_sid28(sid, flags);
}

/*
  return the wire size of a security_ace
*/
size_t ndr_size_security_ace(const struct security_ace *ace, int flags)
{
	if (!ace) return 0;
	return 8 + ndr_size_dom_sid(&ace->trustee, flags);
}


/*
  return the wire size of a security_acl
*/
size_t ndr_size_security_acl(const struct security_acl *acl, int flags)
{
	size_t ret;
	int i;
	if (!acl) return 0;
	ret = 8;
	for (i=0;i<acl->num_aces;i++) {
		ret += ndr_size_security_ace(&acl->aces[i], flags);
	}
	return ret;
}

/*
  return the wire size of a security descriptor
*/
size_t ndr_size_security_descriptor(const struct security_descriptor *sd, int flags)
{
	size_t ret;
	if (!sd) return 0;
	
	ret = 20;
	ret += ndr_size_dom_sid(sd->owner_sid, flags);
	ret += ndr_size_dom_sid(sd->group_sid, flags);
	ret += ndr_size_security_acl(sd->dacl, flags);
	ret += ndr_size_security_acl(sd->sacl, flags);
	return ret;
}

/*
  print a dom_sid
*/
void ndr_print_dom_sid(struct ndr_print *ndr, const char *name, const struct dom_sid *sid)
{
	ndr->print(ndr, "%-25s: %s", name, dom_sid_string(ndr, sid));
}

void ndr_print_dom_sid2(struct ndr_print *ndr, const char *name, const struct dom_sid *sid)
{
	ndr_print_dom_sid(ndr, name, sid);
}

void ndr_print_dom_sid28(struct ndr_print *ndr, const char *name, const struct dom_sid *sid)
{
	ndr_print_dom_sid(ndr, name, sid);
}

void ndr_print_dom_sid0(struct ndr_print *ndr, const char *name, const struct dom_sid *sid)
{
	ndr_print_dom_sid(ndr, name, sid);
}

enum ndr_err_code ndr_push_id_mapping(struct ndr_push *ndr, int ndr_flags, enum id_mapping r)
{
	NDR_CHECK(ndr_push_uint32(ndr, NDR_SCALARS, r));
	return NDR_ERR_SUCCESS;
}

enum ndr_err_code ndr_pull_id_mapping(struct ndr_pull *ndr, int ndr_flags, enum id_mapping *r)
{
	uint32_t v;
	NDR_CHECK(ndr_pull_uint32(ndr, NDR_SCALARS, &v));
	*r = v;
	return NDR_ERR_SUCCESS;
}

void ndr_print_id_mapping(struct ndr_print *ndr, const char *name, enum id_mapping r)
{
	const char *val = NULL;

	switch (r) {
		case ID_UNKNOWN: val = "ID_UNKNOWN"; break;
		case ID_MAPPED: val = "ID_MAPPED"; break;
		case ID_UNMAPPED: val = "ID_UNMAPPED"; break;
		case ID_EXPIRED: val = "ID_EXPIRED"; break;
	}
	ndr_print_enum(ndr, name, "ENUM", val, r);
}

enum ndr_err_code ndr_push_id_type(struct ndr_push *ndr, int ndr_flags, enum id_type r)
{
	NDR_CHECK(ndr_push_uint32(ndr, NDR_SCALARS, r));
	return NDR_ERR_SUCCESS;
}

enum ndr_err_code ndr_pull_id_type(struct ndr_pull *ndr, int ndr_flags, enum id_type *r)
{
	uint32_t v;
	NDR_CHECK(ndr_pull_uint32(ndr, NDR_SCALARS, &v));
	*r = v;
	return NDR_ERR_SUCCESS;
}

void ndr_print_id_type(struct ndr_print *ndr, const char *name, enum id_type r)
{
	const char *val = NULL;

	switch (r) {
		case ID_TYPE_NOT_SPECIFIED: val = "ID_TYPE_NOT_SPECIFIED"; break;
		case ID_TYPE_UID: val = "ID_TYPE_UID"; break;
		case ID_TYPE_GID: val = "ID_TYPE_GID"; break;
	}
	ndr_print_enum(ndr, name, "ENUM", val, r);
}

enum ndr_err_code ndr_push_unixid(struct ndr_push *ndr, int ndr_flags, const struct unixid *r)
{
	if (ndr_flags & NDR_SCALARS) {
		NDR_CHECK(ndr_push_align(ndr, 8));
		NDR_CHECK(ndr_push_uint32(ndr, NDR_SCALARS, r->id));
		NDR_CHECK(ndr_push_id_type(ndr, NDR_SCALARS, r->type));
	}
	if (ndr_flags & NDR_BUFFERS) {
	}
	return NDR_ERR_SUCCESS;
}

enum ndr_err_code ndr_pull_unixid(struct ndr_pull *ndr, int ndr_flags, struct unixid *r)
{
	if (ndr_flags & NDR_SCALARS) {
		NDR_CHECK(ndr_pull_align(ndr, 8));
		NDR_CHECK(ndr_pull_uint32(ndr, NDR_SCALARS, &r->id));
		NDR_CHECK(ndr_pull_id_type(ndr, NDR_SCALARS, &r->type));
	}
	if (ndr_flags & NDR_BUFFERS) {
	}
	return NDR_ERR_SUCCESS;
}

void ndr_print_unixid(struct ndr_print *ndr, const char *name, const struct unixid *r)
{
	ndr_print_struct(ndr, name, "unixid");
	ndr->depth++;
	ndr_print_uint32(ndr, "id", r->id);
	ndr_print_id_type(ndr, "type", r->type);
	ndr->depth--;
}

enum ndr_err_code ndr_push_id_map(struct ndr_push *ndr, int ndr_flags, const struct id_map *r)
{
	if (ndr_flags & NDR_SCALARS) {
		NDR_CHECK(ndr_push_align(ndr, 8));
		NDR_CHECK(ndr_push_unique_ptr(ndr, r->sid));
		NDR_CHECK(ndr_push_unixid(ndr, NDR_SCALARS, &r->xid));
		NDR_CHECK(ndr_push_id_mapping(ndr, NDR_SCALARS, r->status));
	}
	if (ndr_flags & NDR_BUFFERS) {
		if (r->sid) {
			NDR_CHECK(ndr_push_dom_sid(ndr, NDR_SCALARS|NDR_BUFFERS, r->sid));
		}
	}
	return NDR_ERR_SUCCESS;
}

enum ndr_err_code ndr_pull_id_map(struct ndr_pull *ndr, int ndr_flags, struct id_map *r)
{
	uint32_t _ptr_sid;
	TALLOC_CTX *_mem_save_sid_0;
	if (ndr_flags & NDR_SCALARS) {
		NDR_CHECK(ndr_pull_align(ndr, 8));
		NDR_CHECK(ndr_pull_generic_ptr(ndr, &_ptr_sid));
		if (_ptr_sid) {
			NDR_PULL_ALLOC(ndr, r->sid);
		} else {
			r->sid = NULL;
		}
		NDR_CHECK(ndr_pull_unixid(ndr, NDR_SCALARS, &r->xid));
		NDR_CHECK(ndr_pull_id_mapping(ndr, NDR_SCALARS, &r->status));
	}
	if (ndr_flags & NDR_BUFFERS) {
		if (r->sid) {
			_mem_save_sid_0 = NDR_PULL_GET_MEM_CTX(ndr);
			NDR_PULL_SET_MEM_CTX(ndr, r->sid, 0);
			NDR_CHECK(ndr_pull_dom_sid(ndr, NDR_SCALARS|NDR_BUFFERS, r->sid));
			NDR_PULL_SET_MEM_CTX(ndr, _mem_save_sid_0, 0);
		}
	}
	return NDR_ERR_SUCCESS;
}

void ndr_print_id_map(struct ndr_print *ndr, const char *name, const struct id_map *r)
{
	ndr_print_struct(ndr, name, "id_map");
	ndr->depth++;
	ndr_print_ptr(ndr, "sid", r->sid);
	ndr->depth++;
	if (r->sid) {
		ndr_print_dom_sid(ndr, "sid", r->sid);
	}
	ndr->depth--;
	ndr_print_unixid(ndr, "xid", &r->xid);
	ndr_print_id_mapping(ndr, "status", r->status);
	ndr->depth--;
}
